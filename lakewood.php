<!DOCTYPE html>
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="author"      content="Milan (perceptionmm.com)">
	<meta name="description" content="At Lakewood O'Neill Healthcare we work to promote a healthy and active lifestyle for each and every resident. ">
	<meta name="keywords" content="O'Neill Healthcare, O'Neill Managment, O'Neill Nursing Home, Lakewood, Assisted Living, Dialysis, Hospice, Independent Living, Memory Support, Rehab and Therapy, Skilled Nursing, Alzheimer's, Cleveland, Cleveland Nursing Homes, Cleveland Healthcare" />

	<title>Lakewood - O'Neill Healthcare &amp; Managment</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="favicon/browserconfig.xml">
	
	<!-- FancyBox -->
    <link rel="stylesheet" href="assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lte IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<link rel="stylesheet" href="assets/css/ie.css">
	<![endif]-->

	<?php $page = "lake"; ?>

</head>


<body onload="initialize()">


	<!-- Fixed navbar -->
	<?php include 'inc/nav.php'; ?>
	<!-- /.navbar -->


	<header id="head" class="lakewood"></header>

	<!-- container -->
	<div class="container">
		
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="locations.php">Locations</a></li>
			<li class="active">Lakewood</li>
		</ol>

		<div class="row">
			
			<!-- Sidebar -->
			<aside class="col-md-4 sidebar sidebar-left">



				<div class="row widget">
					<div class="col-xs-12">
						<p><img src="assets/images/logos/lakewood_logo.png" alt="O'Neill Healthcare Lakewood Logo" class="img-responsive full-img"></p>
						
					</div>
				</div>

				<div class="row widget">
					<div class="col-xs-12">
						<ul class="mini-nav row">
							
							<li class="call col-md-12">
								<a href="tel:+2162287650">
									<div class="btn btn-default btn-block">
										<div class="phonehide">
											<i class="fa fa-phone fa-2"></i>    Call 
										</div>
										<div class="phone-num">216-228-7650</div>
									</div> 
								</a>
								
							</li>

							<li class="visit col-md-12" >
								<a href="#map_canvas">
								<div class="btn btn-default btn-block">
										<i class="fa fa-location-arrow fa-2"></i>    Visit
								</div>
								</a>
							</li>

							<li class="write col-md-12">
								<div class="btn btn-default btn-block">
									<i class="fa fa-pencil fa-2"></i>     Write
								</div>
							</li>

							<li class="col-md-12">
								<a href="assets/newsletter/lakewood/newsletter.pdf" target="blank">
									<div class="btn btn-default btn-block">
										<i class="fa fa-envelope fa-2"></i>    Newsletter
									</div>
								</a>
							</li>
						
						</ul>

						<div class="bbform form row">

		                  	<div class="form-group">

			                  	
			                  	
			                    <input class="form-control" type="text" name="name" id="name" placeholder="Name">
			                   	
			                    <input class="form-control" type="text" name="email" id="email" placeholder="Email">

			                    <input class="form-control" type="text" name="phone" id="phone" placeholder="Phone">
			                    
			                    <textarea name="message" id="message" rows="7" class="form-control" placeholder="Comments / Questions"></textarea>
			                    <div id="result"></div>

		                	</div>

		                   	<button id="submit_btn" type="submit" class="btn btn-default btn-block">Submit</button>



						</div>
						
						
					</div>
				</div>
				<div class="row">
				<div class="widget col-sm-6 col-md-12 col-lg-12">

					<img src="assets/images/rooms/lakewood_room_1.jpg" alt="Lakewood Room" class="img-responsive full-img"/>
				</div>
				
				<div class="widget col-sm-6 col-md-12 col-lg-12">

					<img src="assets/images/rooms/lakewood_room_2.jpg" alt="Lakewood Room" class="img-responsive full-img"/>
					

				</div>
				<div class="widget col-sm-6 col-md-12 col-lg-12">

					<img src="assets/images/rooms/lakewood_room_3.jpg" alt="Lakewood Room" class="img-responsive full-img"/>
					

				</div>

				<div class="widget col-sm-6 col-md-12 col-lg-12">
					<a href="assets/images/rooms/LK.pdf" target="top">
						<img src="assets/images/rooms/floor_plans_onhc.jpg" alt="Lakewood Floor Plan" class="img-responsive full-img"/>
					</a>

				</div>
				</div>
				
				

			</aside>
			<!-- /Sidebar -->

			<!-- Article main content -->
			<article class="col-md-8 maincontent locmaincontent">
				<header class="page-header">
					<h1 class="page-title centered">Welcome to O'Neill Healthcare Lakewood</h1>
				</header>

				<div class="row">
					
					<p>
						Family owned and operated for more than 50 years, O’Neill Healthcare has been providing comprehensive inpatient and outpatient rehabilitation and nursing care since the opening of its first facility in Bay Village in 1962.
					</p>

					<p>
						Formerly serving the community as Lakewood Senior Health Campus, O’Neill Healthcare acquired this facility in 2006; it became O’Neill Healthcare’s fourth location on Greater Cleveland’s west side. Conveniently located at the corner of Bunts and Detroit avenues in one of the city’s nicest neighborhoods, this facility is nestled in the heart of beautiful and bustling Lakewood, close to shopping, public transportation, public parks overlooking Lake Erie, Cleveland Clinic’s Lakewood Hospital and other medical and dental facilities, churches, dining, hotels and much more.
					</p>

					<p>
						O’Neill Healthcare Lakewood’s award-winning, diverse campus offers multiple levels of care to residents, from independent apartments to full-time nursing care. Residents of O’Neill Healthcare Lakewood never have to leave their familiar surroundings as the level of care they require changes. This saves residents the stress and trauma of relocating to a new facility as their needs change. At O’Neill Healthcare Lakewood they will be able to enjoy the same high-quality staff, physicians and friends even as their rehabilitation or care needs vary.
					</p>

					<p>
						Proudly, O’Neill Healthcare’s Lakewood facility is regularly recognized as one of the finest health care centers in Northeastern Ohio, ranking in the top 10 percent of all nursing homes in the State of Ohio for resident satisfaction.
					</p>

				</div>

				<div class="row">
					<h4 class="list-title">
						Learn more about our services!
					</h4>

					<div class="col-sm-4">
						<div class="service-btn">
							<a href="services/skilled-nursing.php">
								<p>
									Skilled <br>Nursing
								</p>
							</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="service-btn">
							<a href="services/assisted-living.php">
								<p>
									Assisted <br>Living
								</p>
							</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="service-btn">
							<a href="services/independent-living.php">
								<p>
									Independent <br>Living
								</p>
							</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="service-btn">
							<a href="services/rehab-and-therapy.php">
								<p>
									Rehabilitative <br>Therapies
								</p>
							</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="service-btn">
							<a href="services/hospice.php">
								<p>
									Hospice <br>Care
								</p>
							</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="service-btn">
							<a href="services/dialysis.php">
								<p>
									Dialysis <br>Treatment
								</p>
							</a>
						</div>
					</div>
				</div>

				<div class="row">
					
					<h3 class="list-title">
						O’Neill Healthcare Lakewood at a glance:
					</h3>

					<p>
						O’Neill Healthcare Lakewood, unique among other facilities in the area, offers a complete range of rehabilitation and nursing care services for clients of all ages and needs: skilled nursing services, independent and assisted living suites, rehabilitative therapy gym, memory-support services, hospice services and on-site dialysis.
					</p>

					<p>
						O’Neill Healthcare Lakewood features 60 independent living suites, designed to meet the needs of individuals who enjoy the privacy of their own apartment but appreciate the fact that a competent medical team is close at hand.
					</p>

					<p>
						O’Neill Healthcare Lakewood offers 54 assisted living suites and services, which offer both independence and the security in knowing there is a competent medical team on site.
					</p>

					<p>
						O’Neill Healthcare Lakewood features 135 skilled nursing beds. This unit gives residents the nursing and rehabilitative care they need to help them maximize their strength and mobility so they can return home as soon as possible.
					</p>

					<p>
						O’Neill Healthcare Lakewood offers complete orthopedic rehabilitation services, including physician consultation, evaluation and post-surgical follow-ups.
					</p>

					<p>
						O’Neill Healthcare Lakewood offers 11 on-site dialysis stations.
					</p>

				</div>

				<div class="row quickinfo">
					
					<div class="col-sm-6">
						<p>
							O'Neill Healthcare Lakewood<br>
							13900 Detroit Avenue<br>
							Lakewood, OH 44107
						</p>
					</div>
					<div class="col-sm-6">
						<p>
							P 216.228.7650 <br>
							<a href="mailto:info@oneillhc.com">info@oneillhc.com</a>
						</p>
					</div>

				</div>
			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->
	
	<div id="map_canvas"></div>
    <div id="directionsPanel"></div>

	<footer id="footer">

		<?php include 'inc/footer.php'; ?>
	</footer>	
		




	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
	<script src="assets/js/jquery.placeholder.js"></script>
	
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
		var geocoder;
		var directionsService;
		var directionsDisplay;

		function initialize() {
		    var latlng = new google.maps.LatLng(41.486000, -81.788701);
		    var myOptions = {
		        zoom: 13,
		        center: latlng,
		        mapTypeId: google.maps.MapTypeId.ROADMAP,
		        styles: [
				{
					featureType: "landscape",
					stylers: [
					{ color: "#476B00" },
					{ saturation: -50 },
					{ lightness: 70 },
					{ visibility: "on" }
					]
				},{
					featureType: "poi",
					elementType: "labels",
					stylers: [
					{ color: "#194719"},
					{ visibility: "simplified" }
					]
				},{
					featureType: "road.highway",
					stylers: [
					{ saturation: -50 },
					{ visibility: "simplified" }
					]
				},{
					featureType: "road.arterial",
					stylers: [
					{ saturation: -50 },
					{ visibility: "on" }
					]
				},{
					featureType: "road.local",
					stylers: [
					{ saturation: -50 },
					{ lightness: 40 },
					{ visibility: "on" }
					]
				},{
					featureType: "transit",
					stylers: [
					{ saturation: 0 },
					{ visibility: "simplified" }
					]
				},{
					featureType: "administrative.province",
					stylers: [
					{ visibility: "off" }
					]
			/** /
				},{
					featureType: "administrative.locality",
					stylers: [
						{ visibility: "off" }
					]
				},{
					featureType: "administrative.neighborhood",
					stylers: [
						{ visibility: "on" }
					]
					/**/
				},{
					featureType: "water",
					elementType: "labels",
					stylers: [
					{ visibility: "on" },
					{ lightness: -25 },
					{ saturation: -100 }
					]
				},{
					featureType: "water",
					elementType: "geometry",
					stylers: [
					{ color: "#75D1FF"},
					]
				}
				],
		    };

		    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		    var marker = new google.maps.Marker({
		        position: new google.maps.LatLng(41.486000, -81.788701),
		        map: map,
		        title: 'Center Ridge',
		        clickable: true
		    });

		    var infowindow = new google.maps.InfoWindow({
		        content: "Your address: <input id='clientAddress' type='text'>"+
		                "<input type='button' onClick=getDir() value='Go!'>"
		    });

		    google.maps.event.addListener(marker, 'click', function () {
		        infowindow.open(map, marker);
		    });
		    geocoder = new google.maps.Geocoder();
		    directionsService = new google.maps.DirectionsService();
		    directionsDisplay = new google.maps.DirectionsRenderer({
		        suppressMarkers: false
		    });
		    directionsDisplay.setMap(map);
		    
		    directionsDisplay.setPanel(document.getElementById("directionsPanel"));
		   	
		}

		function getDir() {
		    geocoder.geocode({
		        'address': document.getElementById('clientAddress').value
		    },

		    function (results, status) {
		        if (status == google.maps.GeocoderStatus.OK) {
		            var origin = results[0].geometry.location;
		            var destination = new google.maps.LatLng(41.486000, -81.788701);

		            var request = {
		                origin: origin,
		                destination: destination,
		                travelMode: google.maps.DirectionsTravelMode.DRIVING
		            };

		            directionsService.route(request, function (response, status) {
		                if (status == google.maps.DirectionsStatus.OK) {
		                    directionsDisplay.setDirections(response);
		                }
		            });

		        } else {
		            document.getElementById('clientAddress').value =
		                "Directions cannot be computed at this time.";
		        }
		    });
		}
    </script>

    <script>
	
		$(".phone-num").hide();
		$(".call").click(function(){
			$(".phone-num").show();
			$(".phonehide").hide();
		});

		$(".bbform").hide();
		$(".write").click(function(){
			$(".mini-nav").css("border-bottom" , "none");
			$(".bbform").slideDown(1000);

		});

		// placeholder ie fix
		$('input, textarea').placeholder();

		 (function ($) {
                "use strict";

                // Detecting IE
                var oldIE;
                if ($('html').is('.ie6, .ie7, .ie8')) {
                    oldIE = true;
                }

                if (oldIE) {
                    jQuery('label').show();
                } else {
                    jQuery('label').hide();
                }

            }(jQuery));

            /* Form Submition Script */

            $("#submit_btn").click(function() { 

            //$('#result')[0].scrollIntoView(true);

            //get input field values
            var user_name      	= $('input[name=name]').val(); 
            var user_email      = $('input[name=email]').val();
            var user_phone      = $('input[name=phone]').val();
            var user_message    = $('textarea[name=message]').val();
            
            //simple validation at client's end
            //we simply change border color to red if empty field using .css()
            var proceed = true;
            if(user_name==""){ 
                $('input[name=name]').css('border-color','red'); 
                proceed = false;
            }
            if(user_email==""){ 
                $('input[name=email]').css('border-color','red'); 
                proceed = false;
            }
            if(user_phone=="") {    
                $('input[name=phone]').css('border-color','red'); 
                proceed = false;
            }
            if(user_message=="") {  
                $('textarea[name=message]').css('border-color','red'); 
                proceed = false;
            }
            
            //everything looks good! proceed...
            if(proceed) 
            {
                //data to be sent to server
                post_data = {'userName':user_name, 'userEmail':user_email, 'userPhone':user_phone, 'userMessage':user_message};
                
                //Ajax post data to server
                $.post('contact_me.php', post_data, function(data){  
                    
                    //load success massage in #result div element, with slide effect.       
                    $("#result").hide().html('<div class="success">'+data+'</div>').slideDown();
                    
                    //reset values in all input fields
                    $('#contact_form input').val(''); 
                    $('#contact_form textarea').val(''); 
                    
                }).fail(function(err) {  //load any error data
                    $("#result").hide().html('<div class="error">'+err.statusText+'</div>').slideDown();
                });
            }
                    
        });
        
        //reset previously set border colors and hide all message on .keyup()
        $("#contact_form input, #contact_form textarea").keyup(function() { 
            $("#contact_form input, #contact_form textarea").css('border-color',''); 
            $("#result").slideUp();
        });

		// smooth scroll
		 $(function() {
          $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top
                }, 1000);
                return false;
              }
            }
          });
        });
	</script>

	<!-- Google Maps 
	<script src="https://maps.googleapis.com/maps/api/js?key=&amp;sensor=false&amp;extension=.js"></script> 
	<script src="assets/js/google-map.js"></script>
	-->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55359204-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>