<!DOCTYPE html>
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="author"      content="Milan (perceptionmm.com)">
	<meta name="description" content="At O'Neill Healthcare we provide the highest quality of life to each resident by utilizing a holistic approach that involves family and community. We encourage active participation in a progressive rehabilitation plan. All of this takes place in a caring, supportive, and homelike atmosphere.">
	<meta name="keywords" content="O'Neill Healthcare, O'Neill Managment, O'Neill Nursing Home, Bradley Bay, Center Ridge, Lakewood, Wellington, Assisted Living, Dialysis, Hospice, Independent Living, Memory Support, Rehabilitative Therapy, Skilled Nursing, Alzheimer's, Cleveland, Cleveland Nursing Homes, Cleveland Healthcare" />

	<title>Services - O'Neill Healthcare &amp; Managment</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="favicon/browserconfig.xml">
	
	<!-- FancyBox -->
    <link rel="stylesheet" href="assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lte IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<link rel="stylesheet" href="assets/css/ie.css">
	<![endif]-->

	<?php $page = "srv"; ?>
</head>

<body>
	<!-- Fixed navbar -->
	<?php include 'inc/nav.php'; ?>
	<!-- /.navbar -->

	<header id="head" class="services"></header>

	<!-- container -->
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li class="active">Services</li>
		</ol>
		<div class="row">
			<!-- Article main content -->
			<article class="col-md-9 maincontent">
				<header class="page-header">
					<h1 class="page-title">Excellence in Care</h1>
				</header>
				<p>
					Our mission is to provide the highest quality of life to each resident by utilizing a holistic approach that involves family and community. We encourage active participation in a progressive rehabilitation plan. All of this takes place in a caring, supportive, and homelike atmosphere.
				</p>

				<div class="pic-row row">
					
					<div class="col-sm-6">
						<h3 class="highlight center services">Discover more about the services offered at O'Neill Healthcare.</h3>
						<img class="fiftyyears center-img hidden-lg hidden-md" src="assets/images/logos/50yrs.png" alt="O'Neill Healthcare - 50 Years of providing quality care.">
					</div>

					<div class="col-sm-6">
						<a href="services/assisted-living.php">
							<img src="assets/images/service-page/assisted-living.jpg" onmouseover="this.src='assets/images/service-page/assisted-living-hover.jpg'" onmouseout="this.src='assets/images/service-page/assisted-living.jpg'" alt="Assisted Living at O'Neill Healthcare">
						</a>
					</div>

				</div>
				
				<div class="pic-row row">
					
					<div class="col-sm-6">
						<a href="services/dialysis.php">
							<img src="assets/images/service-page/dialysis.jpg" onmouseover="this.src='assets/images/service-page/dialysis-hover.jpg'" onmouseout="this.src='assets/images/service-page/dialysis.jpg'" alt="Dialysis at O'Neill Healthcare">
						</a>
					</div>

					<div class="col-sm-6">
						<a href="services/hospice.php">
							<img src="assets/images/service-page/hospice.jpg" onmouseover="this.src='assets/images/service-page/hospice-hover.jpg'" onmouseout="this.src='assets/images/service-page/hospice.jpg'" alt="Hospice Care at O'Neill Healthcare">
						</a>
					</div>

				</div>

				<div class="pic-row row">
					
					<div class="col-sm-6">
						<a href="services/independent-living.php">
							<img src="assets/images/service-page/independent-living.jpg" onmouseover="this.src='assets/images/service-page/independent-living-hover.jpg'" onmouseout="this.src='assets/images/service-page/independent-living.jpg'" alt="Independent Living at O'Neill Healthcare">
						</a>
					</div>

					<div class="col-sm-6">
						<a href="services/memory-support.php">
							<img src="assets/images/service-page/memory-support.jpg" onmouseover="this.src='assets/images/service-page/memory-support-hover.jpg'" onmouseout="this.src='assets/images/service-page/memory-support.jpg'" alt="Memory Support at O'Neill Healthcare">
						</a>
					</div>

				</div>

				<div class="pic-row row">

					<div class="col-sm-6">
						<a href="services/rehab-and-therapy.php">
							<img src="assets/images/service-page/rehab.jpg" onmouseover="this.src='assets/images/service-page/rehab-hover.jpg'" onmouseout="this.src='assets/images/service-page/rehab.jpg'" alt="Rehab and Therapy at O'Neill Healthcare">
						</a>
					</div>

					<div class="col-sm-6">
						<a href="services/skilled-nursing.php">
							<img src="assets/images/service-page/skilled-nursing.jpg" onmouseover="this.src='assets/images/service-page/skilled-nursing-hover.jpg'" onmouseout="this.src='assets/images/service-page/skilled-nursing.jpg'" alt="Skilled Nursing at O'Neill Healthcare">
						</a>
					</div>

				</div>

			</article>
			<!-- /Article -->	

			<!-- Sidebar -->
			<aside class="col-md-3 sidebar sidebar-right hidden-sm hidden-xs">
				<img class="fiftyyears" src="assets/images/logos/50yrs.png" alt="O'Neill Healthcare - 50 Years of providing quality care.">
			</aside>

			<!-- Article main content -->
			<article class="col-md-12 maincontent">	

				<div class="snc">
					<h2>Attending Physicians:</h2>
					<div class="row">
						<div class="col-md-12">
							<p>One of the most critical components of an effective, individualized skilled nursing care plan is a physician who fully understands a patient’s medical condition and delivers a high level of care, compassion, and communication during regular visits. When residents are admitted to an O’Neill Healthcare facility, they are encouraged to have their family physician participate in their care during their stay. If the family physician does not follow, we can recommend one of several physicians who are available to visit frequently and be involved in your care.</p>
						</div>
					</div>
				</div>

				<div class="trc">
					<h2>Consulting Physicians:</h2>
					<div class="row">
						<div class="col-md-12">
							<p>Residents keep the specialists who they have seen prior to coming to an O'Neill Healthcare facility. We make every effort to communicate with specialists and coordinate care and physician visits to ensure the highest quality results.</p>
						</div>
					</div>

				</div>

				<div class="snc">
					<h2>Hospital Care:</h2>
					<div class="row">
						<div class="col-md-12">
							<p>Residents who require a transfer to a hospital are admitted to the hospital where they stayed prior to being admitted to an O’Neill Healthcare facility, unless they prefer a different option. When returning to the hospital, residents can rest assured that the continuity in their care will remain and continue in the hospital. O’Neill Healthcare has fostered longstanding partnerships with local hospitals to achieve the best outcomes for each resident’s care and recovery.</p>
						</div>
					</div>
				</div>

				<div class="trc">
					<h2>Satisfaction Surveys:</h2>
					<div class="row">
						<div class="col-md-12">
							<p>Our mission is to provide the highest quality of life to each resident by utilizing a holistic approach that involves family and community. We conduct quarterly company-specific satisfaction surveys to ensure that we are exceeding the expectations of our residents, families and employees. We consistently earn high marks on Family Satisfaction &amp; Resident Satisfaction surveys conducted by the state of Ohio.</p>
						</div>
					</div>
				</div>

				<div class="snc">
					<h2>State and Federal Surveys:</h2>
					<div class="row">
						<div class="col-md-12">
							<p>O’Neill Healthcare facilities are certified and licensed by the Ohio Department of Health’s Division of Quality Assurance. This department ensures that the quality of life and care of our residents is maintained by conducting unannounced onsite surveys for compliance with state and federal rules and regulations for nursing facilities. O’Neill Healthcare facilities are regularly recognized as top providers in the state.</p>
						</div>
					</div>
				</div>

				<div class="trc">
					<h2>Electronic Information Sharing:</h2>
					<div class="row">
						<div class="col-md-12">
							<p>Our facilities receive and share electronic information with hospitals and other healthcare providers during the admission process and throughout their stay. This ensures that our nurses and other caregivers have the most up-to-date and accurate information available about residents when formulating their individualized care plans.</p>
						</div>
					</div>
				</div>

			</article> <!-- /Article -->
		</div>
	</div>	<!-- /container -->

	<footer id="footer" class="top-space">

		<?php include 'inc/footer.php'; ?>

	</footer>	
	
	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55359204-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>