

<!DOCTYPE html>
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="author"      content="Milan (perceptionmm.com)">
	<meta name="description" content="At O'Neill Healthcare we provide the highest quality of life to each resident by utilizing a holistic approach that involves family and community.">
	<meta name="keywords" content="O'Neill Healthcare, O'Neill Managment, O'Neill Nursing Home, Bradley Bay, Center Ridge, Lakewood, Wellington, Assisted Living, Dialysis, Hospice, Independent Living, Memory Support, Rehab and Therapy, Skilled Nursing, Alzheimer's, Cleveland, Cleveland Nursing Homes, Cleveland Healthcare, care, living, quality, home, need" />

	<title>O'Neill Healthcare &amp; Managment - Quality, Care, Support, Happiness</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="favicon/browserconfig.xml">
	
	<!-- FancyBox -->
    <link rel="stylesheet" href="assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lte IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<link rel="stylesheet" href="assets/css/ie.css">
	<![endif]-->

	<?php $page = "home"; ?>
</head>

<body class="home">

	<?php include 'inc/nav.php'; ?>

	<!-- Header -->
	<header id="head" class="indexhome">
		<div class="container">
			<div class="row">
				<div class="titles">
					<div class="overlay">
						<h1 class="lead no-textshadow">O'Neill Healthcare</h1>
						<p class="tagline no-textshadow">Celebrating 50 years of providing quality care</p>

						<p>
							<a href="contact.php" class="btn btn-default btn-lg" role="button">Schedule a Tour</a> 
							<a class="btn btn-action btn-lg fancybox-media no-cssgradients" role="button" href="https://www.youtube.com/watch?v=f5HocMLfdq0" >Video Guide</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</header> <!-- /Header -->


	<!-- Highlights -->
	<div class="jumbotron">
		<div class="container-fluid">
			<h2 class="text-center thin">Reasons to choose us</h2>

			<!--For Large Devices Only-->

			<div class="row hidden-md hidden-sm hidden-xs">
				<div class="col-lg-3">			
					<div class="front-page-pic">
						<!-- <span class="text-over-img"><span>Quality means doing it right when no one is looking.</span></span> -->
						
						<img src="assets/images/quality.jpg" alt="elderly man receiving physical therapy" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Quality</h2>
				</div>
				<div class="col-lg-3">					
					<div class="front-page-pic">
						<!-- <span class="text-over-img"><span>The greatest joys in life are found not only in what we do and feel, but also in our quiet hopes and labors for others. </span></span> -->
						
						<img src="assets/images/support.jpg" alt="elderly couple walking together" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Support</h2>
				</div>
				<div class="col-lg-3">	
					<div class="front-page-pic">
						<!-- <span class="text-over-img"><span>If everyone is moving forward together, then success takes care of itself.</span></span> -->
						<img src="assets/images/care.jpg" alt="older women with a smiling nurse" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Care</h2>
				</div>
				<div class="col-lg-3">
					<div class="front-page-pic">
						<!-- <span class="text-over-img"><span>It is not how much we have, but how much we enjoy, that makes happiness.</span></span> -->
						<img src="assets/images/happiness.jpg" alt="older women with a smiling nurse" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Happiness</h2>
				</div>
			</div> <!-- /row  -->

			<!--For Medium and Small Devices-->

			<div class="row hidden-lg hidden-xs">
				<div class="col-sm-6">			
					<div class="front-page-pic">
						<span class="text-over-img"><span>Quality means doing it right when no one is looking.</span></span>
						
						<img src="assets/images/quality.jpg" alt="elderly man receiving physical therapy" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Quality</h2>
				</div>
				<div class="col-sm-6">					
					<div class="front-page-pic">
						<span class="text-over-img"><span>The greatest joys in life are found not only in what we do and feel, but also in our quiet hopes and labors for others. </span></span>
						
						<img src="assets/images/support.jpg" alt="elderly couple walking together" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Support</h2>
				</div>
			</div>

			<div class="row hidden-lg hidden-xs">		
				
				<div class="col-sm-6">	
					<div class="front-page-pic">
						<span class="text-over-img"><span>If everyone is moving forward together, then success takes care of itself.</span></span>
						
						<img src="assets/images/care.jpg" alt="older women with a smiling nurse" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Care</h2>
				</div>
				<div class="col-sm-6">
					<div class="front-page-pic">
						<span class="text-over-img"><span>It is not how much we have, but how much we enjoy, that makes happiness.</span></span>
						
						<img src="assets/images/happiness.jpg" alt="older women with a smiling nurse" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Happiness</h2>
				</div>
			</div> <!-- /row  -->

			<!--For Extra Small Devices-->

			<div class="row hidden-lg hidden-md hidden-sm ">
				<div class="col-xs-12">			
					<div class="front-page-pic">
						<img src="assets/images/quality.jpg" alt="elderly man receiving physical therapy" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Quality</h2>
					<h4 class="centered">Quality means doing it right when no one is looking.</h4>
				</div>
				<div class="col-xs-12">					
					<div class="front-page-pic">
						<img src="assets/images/support.jpg" alt="elderly couple walking together" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Support</h2>
					<h4 class="centered">The greatest joys in life are found not only in what we do and feel, but also in our quiet hopes and labors for others. </h4>
				</div>
				<div class="col-xs-12">	
					<div class="front-page-pic">
						<img src="assets/images/care.jpg" alt="older women with a smiling nurse" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Care</h2>
					<h4 class="centered">If everyone is moving forward together, then success takes care of itself.</h4>
				</div>
				<div class="col-xs-12">
					<div class="front-page-pic">
						<img src="assets/images/happiness.jpg" alt="older women with a smiling nurse" class="img-responsive front-page-pic">
					</div>
					<h2 class="h-caption front-page">Happiness</h2>
					<h4 class="centered">It is not how much we have, but how much we enjoy, that makes happiness.</h4>
				</div>
			</div> <!-- /row  -->

		</div> <!-- /container -->
	</div> <!-- /Highlights -->

	<!-- container -->
	<div class="container faq">
		<h3 class="text-center top-space">FAQ</h3>
		<div class="row">
			<div class="col-sm-6">

				<h4 class="triangle-right">My loved one is being discharged from the hospital but isn’t ready to return home. What do I do?</h4>

				<p class="triangle-right top"><a href="contact.php" target="blank">Contact us</a> to speak with an elder care specialist who can help you, free of charge, determine the specific needs of your loved one. Questions such as “What is short-term rehabilitation?” or  "Nursing care vs. assisted living" are decisions we deal with everyday. Let us help you find a place that will meet your loved one’s needs, help them heal, and promote their well-being.</p>
				
			</div>
			<div class="col-sm-6">
				<h4 class="triangle-right">What's the difference between assisted and independent living?</h4>
				<p class="triangle-right top">
					Both facilities provide familiar, homelike living arrangements with a range of optional services tailored to suit your needs.  <a href="services/independent-living.php" target="blank">Independent living</a> is for those who like living and socializing in a senior community, but may need or want access to immediate medical attention.  <a href="services/assisted-living.php" target="blank">Assisted living</a>, on the other hand, provides a lifestyle with access to medical and personal help with everyday activities. 
				</p>
			</div>
		</div> <!-- /row -->
		<div class="row">
			<div class="col-sm-6">
				<h4 class="triangle-right">Why do people need long-term care?</h4>
				<p class="triangle-right top">
					People often need long-term care when they have a serious, ongoing health condition or disability. The need for long-term care can arise suddenly, such as after a heart attack or stroke. Most often, however, it usually develops gradually, as people get older and frailer or as an illness or disability gets worse.
				</p>
			</div>
			<div class="col-sm-6">
				<h4 class="triangle-right">Can I apply to work here?</h4>
				<p class="triangle-right top">
					You can! Visit our <a href="employment.php" target="blank">employment page</a>, and read about how to apply, benefits offered, existing postitions, and print out an application.
				</p>
			</div>
		</div> <!-- /row -->

		<div class="jumbotron top-space centered">
			<h4>
				Join us for an upcoming community event!
			</h4>

			<div class="btn-group">
			  <a href="assets/other/seminar.pdf" target="_blank">
			  	<button type="button" class="btn btn-success btn-large">
			    Upcoming Events
			  	</button>
			  </a>
			</div>

			<h4>
				Check out our monthly newsletters.
			</h4>

			<div class="btn-group">
			  <button type="button" class="btn btn-success btn-large dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    View Our Newsletters <span class="caret"></span>
			  </button>
			<?php
				$bay_dir = "assets/newsletter/bayvillage/";
				$bd  = opendir($bay_dir);
				while (false !== ($bay_filename = readdir($bd))) {
				    $bay_files[] = $bay_filename;
				}

				rsort($bay_files);

				$newest_bay_file = $bay_files[0];

				$fair_dir = "assets/newsletter/fairview/";
				$fd  = opendir($fair_dir);
				while (false !== ($fair_filename = readdir($fd))) {
				    $fair_files[] = $fair_filename;
				}

				rsort($fair_files);

				$newest_fair_file = $fair_files[0];

				$lake_dir = "assets/newsletter/lakewood/";
				$ld  = opendir($lake_dir);
				while (false !== ($lake_filename = readdir($ld))) {
				    $lake_files[] = $lake_filename;
				}

				rsort($lake_files);

				$newest_lake_file = $lake_files[0];

				$olm_dir = "assets/newsletter/northolmsted/";
				$od  = opendir($olm_dir);
				while (false !== ($olm_filename = readdir($od))) {
				    $olm_files[] = $olm_filename;
				}

				rsort($olm_files);

				$newest_olm_file = $olm_files[0];

				$ridge_dir = "assets/newsletter/northridgeville/";
				$rd  = opendir($ridge_dir);
				while (false !== ($ridge_filename = readdir($rd))) {
				    $ridge_files[] = $ridge_filename;
				}

				rsort($ridge_files);

				$newest_ridge_file = $ridge_files[0];
			?>
			  <ul class="dropdown-menu">
			    <li><a target="_blank" href="assets/newsletter/bayvillage/<?php echo "$newest_bay_file"; ?>">Bay Village</a></li>
			    <li><a target="_blank" href="assets/newsletter/fairview/<?php echo "$newest_fair_file"; ?>">Fairview</a></li>
			    <li><a target="_blank" href="assets/newsletter/lakewood/<?php echo "$newest_lake_file"; ?>">Lakewood</a></li>
			    <li><a target="_blank" href="assets/newsletter/northolmsted/<?php echo "$newest_olm_file"; ?>">North Olmsted</a></li>
			    <li><a target="_blank" href="assets/newsletter/northridgeville/<?php echo "$newest_ridge_file"; ?>">North Ridgeville</a></li>
			  </ul>
			</div>

  		</div>
		<div class="jumbotron top-space">
			<h4>
				The O’Neill Healthcare family of senior communities represents a strong, multi-faceted organization that is highly effective in what we do.
			</h4>
			<ul>
				<li>We are committed to delivering the highest quality of care in the most personalized way possible</li>
				<li>We are appreciated by our residents, their families and the communities where our facilities are located</li>
				<li>We are innovative in the services we offer, keeping current with market needs and clinical developments</li>
				<li>We are dedicated to our employees, their well-being and growth as health care professionals</li>
				<li>We are consistently recognized at local, state and national levels for delivering the highest quality of care and realizing high levels of resident satisfaction</li>
			</ul>

     		<p class="text-center"><a href="services.php" class="btn btn-success btn-large">Learn more »</a></p>
  		</div>

  		

	</div>	<!-- /container -->

	<footer id="footer" class="top-space">

		<?php include 'inc/footer.php'; ?>

	</footer>	


	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/modernizr.custom.12036.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
	<script type="text/javascript" src="assets/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script type="text/javascript" src="assets/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript" src="assets/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <script>
    //Start FancyBox
        jQuery('.fancybox-media').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            helpers : {
                media : {}
            }
        });
    </script>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55359204-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<script src="jquery.lazyload.js"></script>
</body>
</html>