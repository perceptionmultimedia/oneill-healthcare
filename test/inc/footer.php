<div class="footer1">
	<div class="container">
		<div class="row">
			
			<!--<div class="col-md-3 widget">
				
				<div class="widget-body">
					<h3 class="widget-title">Contact</h3>
						<a href="tel:440-808-5500"><p>440.808.5500<br></a>
						<a href="mailto:info@oneillhc.com">info@oneillhc.com</a>
						</p>	
				</div>
			</div>-->

			
			<h2 class="widget-title">Our Mission</h2>

			<div class="row">
				<div class="col-sm-2">
					<img src="inc/hud.png" class="img-responsive" alt="HUD logo">
				</div>
				<div class="col-sm-9 widget res-footer">
					<p class="left-align">
						Our mission is to provide the highest quality of life to each resident by utilizing a holistic approach that involves family and community. We encourage active participation in a progressive rehabilitation plan. All of this takes place in a caring, supportive, and homelike atmosphere.
					</p>
				</div>
			</div>

		</div> <!-- /row of widgets -->
	</div>
</div>

<div class="footer2">
	<div class="container">

		<div class="row">
			<h2 class="widget-title">Quick Links</h2>
		</div>

		<div class="row">
			<a href="index.php">
				<div class="col-md-3 footer-links">
					<span class="fa fa-home fa-5x"></span>
					<h4>Home</h4>
				</div>
			</a>

			<a href="about.php">
				<div class="col-md-3 footer-links">
					<span class="fa fa-user-md fa-5x"></span>
					<h4>About</h4>
				</div>
			</a>

			<a href="employment.php">
				<div class="col-md-3 footer-links">
					<i class="fa fa-briefcase fa-5x"></i>
					<h4>Work with Us</h4>
				</div>
			</a>

			<a href="contact.php">
				<div class="col-md-3 footer-links">
					<span class="fa fa-bullhorn fa-5x"></span>
					<h4>Contact</h4>
				</div>
			</a>

			<!--<div class="col-md-6 widget">
				<div class="widget-body">
					<p class="text-right res-footer-txt">
						Copyright &copy; <?php echo date("Y"); ?> Designed by <a href="http://perceptionmm.com/" >Perception Multimedia</a> 
					</p>
				</div>
			</div>-->
		</div> <!-- /row of widgets -->
	</div>
</div>

<div class="footer3">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<p>440.808.5500 <br>
				info@oneillhc.com <br>
				Copyright &copy; <?php echo date("Y"); ?></p>
				<p class="hidden">
					Designed by Perception Multimedia
				</p>
			</div>
		</div>
	</div>
</div>