<!DOCTYPE html>
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="author"      content="Milan (perceptionmm.com)">
	<meta name="description" content="Hospice care is a specialized service focusing on alleviating physical, emotional, and spiritual symptoms of discomfort from a life-limiting illness.">
	<meta name="keywords" content="O'Neill Healthcare, O'Neill Managment, O'Neill Nursing Home, Bradley Bay, Center Ridge, Lakewood, Wellington, Hospice, Cleveland, Cleveland Nursing Homes, Cleveland Healthcare" />

	<title>Hospice - O'Neill Healthcare &amp; Managment</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="../favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="../favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="../favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="../favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="../favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="../favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="../favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="../favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="../favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="../favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="../favicon/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="../favicon/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="../favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="../favicon/browserconfig.xml">
	
	<!-- FancyBox -->
    <link rel="stylesheet" href="../assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="../assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="../assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="../assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="../assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lte IE 9]>
	<script src="../assets/js/html5shiv.js"></script>
	<script src="../assets/js/respond.min.js"></script>
	<link rel="stylesheet" href="../assets/css/ie.css">
	<![endif]-->

	<?php $page = "hospice"; ?>
</head>

<body>
	<!-- Fixed navbar -->
	<?php include '../inc/add/nav.php'; ?>
	<!-- /.navbar -->

	<header id="head" class="hospice"></header>

	<!-- container -->
	<div class="container">

		<ol class="breadcrumb">
			<li><a href="../index.php">Home</a></li>
			<li><a href="../services.php">Services</a></li>
			<li class="active">Hospice</li>
		</ol>

		<div class="row">
			
			<!-- Article main content -->
			<article class="col-md-12 maincontent">
				<header class="page-header">
					<h1 class="page-title">Hospice Care at O’Neill Healthcare</h1>
				</header>

				
				<h3 class="highlight">Compassionate and supportive supplemental care focusing on comfort and quality of life</h3>
				<div class="row">
				<div class="col-md-7">
					<h4>Purpose</h4>

					<p>
						O’Neill Healthcare Hospice provides extra care and comfort to the residents of our facilities and the surrounding area, when they are near the end stages of life. The hospice staff, working closely with the facility staff, becomes part of the team to deliver care and enhance the quality of life of the resident.
					</p>
				
					<h4>Our Mission</h4>

					<p>
						O’Neill Healthcare Hospice’s mission is to help residents achieve the highest level of comfort and quality of life possible while remaining in the security of their own environment.
					</p>

					<h4>What is Hospice Care?</h4>

					<p>
						Hospice care is a specialized service focusing on alleviating physical, emotional, and spiritual symptoms of discomfort due to a life-limiting illness. Comfort care addresses the symptoms of an illness rather than employ aggressive treatments for curative measures. A team of specialists provides guidance and support to both the patient and the family. The hospice staff works with the resident and family to discuss goals, make a plan of care, and facilitate meeting those goals and providing comfort.
					</p>

					<h4 class="list-title">Features of O’Neill Healthcare Hospice Care</h4>

					<ul>
						<li>Our specially trained hospice team includes:</li>
							<ul>
								<li>Nurse case manager</li>
								<li>Hospice physicians</li>
								<li>Nurse practitioners</li>
								<li>Licensed nurses</li>
								<li>State-tested nursing assistants</li>
								<li>Social workers</li>
								<li>Chaplain</li>
								<li>Volunteers</li>
							</ul>
						<li>An on-call staff member is available 24 hours a day, to support the resident, family, and facility staff as needed.</li>
						<li>The family and resident can receive anticipatory grief support and bereavement follow-up.</li>
						<li>Community Health Accreditation Program (CHAP) certified</li>
						<li>Member of the Midwest Care Alliance</li>
					</ul>
				
				</div>
				<div class="col-md-5">
					<img src="../assets/images/hospice2.jpg" alt="Our resident reading a book" />
				</div>	
				</div>

				<div class="row">
					<div class="col-md-7">
					
						<h4 class="list-title">When to choose Hospice Care</h4>

							<p>
								After many different interventions and medications, there may be little to no change noted. When a resident has reached the point where aggressive, curative treatments are no longer effective, it may be time for comfort care with hospice. Signs that hospice care may be needed include:
							</p>
							<ul>
								<li>Decreased appetite with significant weight loss </li>
								<li>Sleeping excessively</li>
								<li>Loss of mobility</li>
								<li>Inability to independently eat or perform personal care</li>
								<li>Increased discomfort/pain</li>
								<li>Little or no interaction/verbal communication</li>
							</ul>
						
						<h4 class="list-title">Who can receive Hospice Care?</h4>

							<p>Many illnesses progress to the point where hospice care is beneficial. General criteria to qualify for hospice care are:</p>

							<ul>
								<li>A physician has issued a diagnosis of a life-limiting illness</li>
								<li>Resident is no longer a candidate for, or no longer wishes to receive, curative treatment</li>
							</ul>

					</div>

					<div class="col-md-5">

						<img src="../assets/images/hospice.jpg" alt="Caring hands" />

					</div>
				</div>
				
				<div class="row">				
					<div class="col-md-7">
						<h4 class="list-title">What is the cost of Hospice Care?</h4>

							<ul>
								<li>Medicare Part A covers hospice services 100%. The Medicare Hospice benefits include personal care from the hospice staff, medical equipment, wound care, medications, counseling, and supplies for the terminal illness and related conditions.</li>
								<li>Many private insurances include hospice coverage, which varies for each policy.</li>
								<li>Out-of-pocket costs, if any, are minimal.</li>
							</ul>

						<h4 class="list-title">How do I find out if Hospice is the right choice?</h4>

							<p>Call O’Neill Healthcare Hospice at (440) 805-5500 and press 6:</p>
							<ul>
								<li>To learn more about hospice services</li>
								<li>If you think you or a loved one might benefit from hospice care</li>
								<li>To set up an informational visit with one of our Hospice team members</li>
							</ul>

						<h4 class="list-title">Helpful resources</h4>
							<ul>
								<li><a href="http://www.medicare.gov/Pubs/pdf/02154.pdf">Hospice Medicare Benefits</a></li>
								<li>Grief support
									<ul>
										<li><a href="http://www.grief.com">grief.com</a></li>
										<li><a href="http://www.griefshare.org">griefshare.org</a></li>
									</ul>
								</li>
								<li>Children’s and Family Grief
									<ul>
										<li><a href="http://www.cornerstoneofhope.org">cornerstoneofhope.org</a></li>
										<li><a href="http://www.joelsplaceforchildren.org">joelsplaceforchildren.org</a></li>
									</ul>
								</li>
								<li>Support Groups
									<ul>
										<li><a href="http://www.211Clevland.org">211Clevland.org</a></li>
										<li><a href="http://www.nami.org">nami.org</a></li>
									</ul>
								</li>
							</ul>
					</div>
				</div>


			</article>
			<!-- /Article -->
			
			

		</div>
	</div>	<!-- /container -->
	

	<footer id="footer" class="top-space">

		<div class="locfooter">
			<div class="container">
				<div class="row">
					<div class="col5 icol widget">
						<div class="locpan">
						<p><a href="../bayvillage.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../bayvillage.php">Bay Village</a></p>
						<p>
							605 Bradley Road<br>
							Bay Village, OH 44140<br> 
						</p>
						<p>
							440-871-3474
						</p>
						</div>
					</div>

					<div class="col5 icol widget ">
						<div class="locpan">
						<p><a href="../fairview.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../fairview.php">Fairview Park</a></p>
						<p>
							20770 Lorain Road<br>
							Fairview Park, OH 44126<br>
						</p>
						<p>
							440-331-0300
						</p>
						</div>
					</div>

					<div class="col5 icol widget">
						<div class="locpan">
						<p><a href="../lakewood.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../lakewood.php">Lakewood</a></p>						
						<p>
							13900 Detroit Avenue<br>
							Lakewood, OH 44107<br>
						</p>
						<p>
							216-228-7650
						</p>
						</div>
					</div>

					<div class="col5 icol widget ">
						<div class="locpan">
						<p><a href="../northolmsted.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../northolmsted.php">North Olmsted</a></p>
						<p>
							4800 Clague Road<br>
							N Olmsted, OH 44070<br>
						</p>
						<p>
							440-734-9933
						</p>
						</div>
					</div>

					<div class="col5 icol icol100 widget ">
						<div class="locpan">
						<p><a href="../northridgeville.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../northridgeville.php">North Ridgeville</a></p>
						<p>
							38600 Center Ridge Road<br>
							N Ridgeville, OH 44039<br>
						</p>
						<p>
							440-327-1295
						</p>
						</div>
					</div>


					

				</div> <!-- /row of widgets -->
			</div>
		</div>
		
		<div class="container">
			<div class="row legal-msg">
				<div class="col-md-2 col-sm-3 col-xs-6">
					<img src="../assets/images/logos/chap_orange.png" alt="Community Health Accreditation Program">
				</div>
				<div class="col-sm-5 col-xs-6">
					<p>
						Patient services are provided without regard to race, color, religion, age, gender, sexual orientation, disability (mental or physical), communicable disease, or place of national origin.
					</p>
				</div>

			</div>
		</div>
		<?php include '../inc/add/footer.php'; ?>

	</footer>	
		


				
				
				




	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="../assets/js/headroom.min.js"></script>
	<script src="../assets/js/jQuery.headroom.min.js"></script>
	<script src="../assets/js/template.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55359204-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>
