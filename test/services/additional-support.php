<!DOCTYPE html>
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="author"      content="Milan (perceptionmm.com)">
	<meta name="description" content="We provide our residents with a personalized care plan to meet their daily living needs. Assistance with everything from bathing, dressing, and ambulating to medication management needs.">
	<meta name="keywords" content="O'Neill Healthcare, O'Neill Managment, O'Neill Nursing Home, Bradley Bay, Center Ridge, Lakewood, Wellington, Assisted Living, Cleveland, Cleveland Nursing Homes, Cleveland Healthcare" />

	<title>Additional Support Programs - O'Neill Healthcare &amp; Managment</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="../favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="../favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="../favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="../favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="../favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="../favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="../favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="../favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="../favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="../favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="../favicon/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="../favicon/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="../favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="../favicon/browserconfig.xml">
	
	<!-- FancyBox -->
    <link rel="stylesheet" href="../assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="../assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="../assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="../assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="../assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lte IE 9]>
	<script src="../assets/js/html5shiv.js"></script>
	<script src="../assets/js/respond.min.js"></script>
	<link rel="stylesheet" href="../assets/css/ie.css">
	<![endif]-->

	<?php $page = "additionalsupport"; ?>
</head>

<body>
	<!-- Fixed navbar -->
	<?php include '../inc/add/nav.php'; ?>
	<!-- /.navbar -->

	<header id="head" class="additional-support"></header>
	<!-- container -->
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="../index.php">Home</a></li>
			<li><a href="../services.php">Services</a></li>
			<li class="active">Additional Support Programs</li>
		</ol>
		<div class="row">
			<!-- Article main content -->
			<article class="col-md-8 maincontent">
				<header class="page-header">
					<h1 class="page-title">Additional Support Programs</h1>
				</header>
					<h3>Heart Failure and Cardiac Rehabilitation</h3>

					<p>A diagnosis of heart failure or experiencing a cardiac event can be a difficult matter and lead to many lifestyle adjustments. Our facilities offer a comprehensive, holistic approach to care and will help you achieve your goals for recovery.</p>

					<p>Our highly educated and specialized therapy staff work with our residents to establish a personalized approach for treatment. They will develop a therapy program to include exercises to maximize your strength and endurance to complete your normal activities of daily living.  If things are a little more challenging than they used to be, they can offer compensatory techniques that will help you continue to participate in activities that you enjoy.</p>

					<p>If dietary changes are recommended by your physician,  our certified dietetic professionals can a you in developing a nutritious meal plan and provide education on foods to avoid in order to maintain optimal health.</p>

					<p>In our Heart Failure program, our nurses are specifically trained to monitor patients to identify minute assessment changes that may need to be addressed by the physician.  Our cardiac program is enhanced by the input and oversight provided by a medical director, who has practiced Northeast Ohio for several years. His expertise and recommendations are shard with the attending physicians for changes to approaches in care.</p>

					<h4 class="list-title">On-site diagnostic testing abilities include:</h4>

					<ul>
						<li>Lab draws</li>
						<li>EKG</li>
						<li>Echocardiography</li>
						<li>Pacemaker checks</li>
						<li>Holter Monitoring</li>
					</ul>

					<p>Our nursing staff will also provide education to you and your family on oxygen use, medications and interactions, and, as needed,  how to monitor lab work ordered by the physician.</p>

					<p>When it is time to return home, our social service staff will ensure you have all the support you need to make your transition home a successful one.</p>

					<h3>Stroke Rehabilitation Program</h3>

					<p>A patients’ abilities following a cerebrovascular accident (CVA), or stroke, can vary widely depending on the location in the brain, type, and severity of the event. For this reason, every stroke patient’s recovery program is different. Our highly skilled therapists evaluate the abilities of each resident and develop a comprehensive rehabilitation program addressing his/her specific needs and goals.</p>

					<p>Speech therapy helps residents address cognition problems, information processing, speech difficulties, and swallowing challenges that may occur after a stroke. Speech therapists determine each resident’s status and develop innovative programs to work towards regaining pre-stroke functions and abilities.</p>

					<p>Physical and Occupational therapy specialists work together to help conquer the challenges residents may experience with fine motor control, muscle weakness, coordination, gait, and mobility. Our therapy gyms contain specialized equipment that address these needs in a variety of ways, enabling our therapists to develop individualized exercise programs to help the residents focus on achieving rehabilitation goals. Through consultations with the patient and family, as well as conducting home evaluations when needed, our physical and occupational therapists will help determine what environmental adaptations are needed to help your loved one successfully transition back home.</p>

					<h4 class="highlight">Recovery for the mind and the body</h4>

					<p>O’Neill Healthcare facilities utilize a holistic approach to recovery. Along with therapy, our nursing staff is trained to monitor for any neurological changes and improvements in the resident’s condition. Our nurses and therapy staff communicate regularly with each other and with physicians regarding our residents’ conditions. Our interdisciplinary teams establish an individualized plan of care for each resident and, once learned by the resident, incorporate these approaches in their daily activities to reinforce the training.</p>

					<p>It is not uncommon for post-stroke patients to experience emotional stress or have difficulty coping with changes. Our compassionate, trained social service staff offers emotional support and make referrals, as needed, to supportive agencies to help meet the needs of the residents. To assist families making the transition to home, our social service staff can provide information and facilitate arrangements with community resources that offer a wide variety of services.</p>

			</article><!-- /Article -->
		
			<!-- Sidebar -->
			<aside class="col-md-4 sidebar sidebar-right">
				<div class="row widget">
					<div class="col-xs-12">
						<p><img src="../assets/images/additional-support.jpg" alt="An O'Neill Healthcare patient getting ready for therapy."/></p>
					</div>
				</div>
			</aside><!-- /Sidebar -->
		</div>
	</div>	<!-- /container -->
	
	<footer id="footer" class="top-space">
		<div class="locfooter">
			<div class="container">
				<div class="row">
					
					<div class="col5 icol widget">
						<div class="locpan">
						<p><a href="../bayvillage.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../bayvillage.php">Bay Village</a></p>
						<p>
							605 Bradley Road<br>
							Bay Village, OH 44140<br>
						</p>
						<p>
							440-871-3474
						</p>
						</div>
					</div>

					<div class="col5 icol widget ">
						<div class="locpan">
						<p><a href="../fairview.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../fairview.php">Fairview Park</a></p>
						<p>
							20770 Lorain Road<br>
							Fairview Park, OH 44126<br>
						</p>
						<p>
							440-331-0300
						</p>
						</div>
					</div>

					<div class="col5 icol widget">
						<div class="locpan">
						<p><a href="../lakewood.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../lakewood.php">Lakewood</a></p>						
						<p>
							13900 Detroit Avenue<br>
							Lakewood, OH 44107<br> 
						</p>
						<p>
							216-228-7650
						</p>
						</div>
					</div>

					<div class="col5 icol widget ">
						<div class="locpan">
						<p><a href="../northolmsted.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../northolmsted.php">North Olmsted</a></p>
						<p>
							4800 Clague Road<br>
							N Olmsted, OH 44070<br>
						</p>
						<p>
							440-734-9933
						</p>
						</div>
					</div>

					<div class="col5 icol icol100 widget ">
						<div class="locpan">
						<p><a href="../northridgeville.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../northridgeville.php">North Ridgeville</a></p>
						<p>
							38600 Center Ridge Road<br>
							N Ridgeville, OH 44039<br>
						</p>
						<p>
							440-327-1295
						</p>
						</div>
					</div>

					

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<?php include '../inc/add/footer.php'; ?>

	</footer>	
		
	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="../assets/js/headroom.min.js"></script>
	<script src="../assets/js/jQuery.headroom.min.js"></script>
	<script src="../assets/js/template.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55359204-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>
