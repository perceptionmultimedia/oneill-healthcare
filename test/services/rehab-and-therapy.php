<!DOCTYPE html>
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="author"      content="Milan (perceptionmm.com)">
	<meta name="description" content="Our rehabilitation programs consist of licensed therapists and assistants using a team approach to help each resident achieve their highest functional status.">
	<meta name="keywords" content="O'Neill Healthcare, O'Neill Managment, O'Neill Nursing Home, Bradley Bay, Center Ridge, Lakewood, Wellington, Rehab and Therapy, Alzheimer's, Cleveland, Cleveland Nursing Homes, Cleveland Healthcare" />

	<title>Rehab and Therapy - O'Neill Healthcare &amp; Managment</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="..//favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="../favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="../favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="../favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="../favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="../favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="../favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="../favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="../favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="../favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="../favicon/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="../favicon/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="../favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="../favicon/browserconfig.xml">
	
	<!-- FancyBox -->
    <link rel="stylesheet" href="../assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="../assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="../assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="../assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="../assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lte IE 9]>
	<script src="../assets/js/html5shiv.js"></script>
	<script src="../assets/js/respond.min.js"></script>
	<link rel="stylesheet" href="../assets/css/ie.css">
	<![endif]-->

	<?php $page = "rehab"; ?>
</head>

<body>
	<!-- Fixed navbar -->
	<?php include '../inc/add/nav.php'; ?>
	<!-- /.navbar -->

	<header id="head" class="therapy"></header>

	<!-- container -->
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="../index.php">Home</a></li>
			<li><a href="../services.php">Services</a></li>
			<li class="active">Therapy &amp; Rehabilitative Care</li>
		</ol>
		<div class="row">
			<!-- Article main content -->
			<article class="col-md-8 maincontent">
				<header class="page-header">
					<h1 class="page-title">Rehabilitative Therapies</h1>
				</header>
					<p>
						Comprehensive rehabilitation is critical component in attaining the best recovery outcomes and regaining the most strength following a surgery, injury, or illness.
					</p>
					<p>
						Our skilled therapists offer compassionate coaching and innovative strategies that enable you or your loved one to achieve the highest quality of life, whether after a hospital stay or for general health. We offer a supportive environment for residents to increase their strength, function, mobility, and confidence that allows them to achieve optimal health and independence.
					</p>
					<p>
						Achieving the maximum level of recovery and rehabilitation for our residents is our primary goal. We promote short-term admissions and encourage residents to return home as soon as they are safely able to do so.
					</p>
					<p>
						Our in-house therapy team consists of physical, occupational, and speech therapists that specialize in post-medical and post-surgical reconditioning and rehabilitation following joint replacements, strokes or neurological disorders. They work hands-on with our residents in therapy gyms that contain state-of-the-art equipment such as Nu-Step recumbent bikes.
					</p>
				<h3 class="highlight">Stronger Together</h3>
					<p>
						A collaborative team approach is the foundation of our rehabilitative programs. Therapists work closely with each resident to address their individual challenges through a progressive, customized rehabilitation plan. Our therapists also work closely with the nursing staff to ensure excellent medical attention and care for our residents in and out of the therapy gym.
					</p>
					<p>
						Residents are active in their treatment plan, and benefit from a focused, consistent team of therapists who employ a holistic strategy to help them achieve their personal goals. Through the hard work and trials encountered in the recovery process, their team strives to inspire them with positive support, respect and understanding.
					</p>
				<h4 class="list-title">On-Site Orthopedic Physician</h4>
					<p>
						O'Neill Healthcare and expert orthopedic professionals have joined forces to establish an Orthopaedic Clinic. This collaborative center affords residents convenient access to physician consultation, evaluation, and post-surgical follow-up without ever leaving our facility. The center’s treatments and services are covered by most insurance plans and Medicare.
					</p>
					<p>
						Extensive therapy services are offered to all residents seven days a week. Prior to discharge, a home visit can be arranged to assess a resident’s surroundings and promote a safe environment. Outpatient therapy services are also available.
					</p>
				<h4 class="list-title">Physical Therapy</h4>
					<p>
						Our physical therapists and assistants help residents regain and retain their strength, flexibility, and balance. They develop personalized strategies using traditional therapy methods and the most current treatments and approaches.
					</p>
					<ul>
						<li>Ambulation</li>
						<li>Transfer training</li>
						<li>Balance and coordination</li>
						<li>Pain management</li>
						<li>Wheelchair positioning and mobility</li>
						<li>Ultrasound and electrical stimulation</li>
						<li>Exercise programs</li>
						<li>Home evaluations</li>
						<li>Family education and training</li>
					</ul>
				<h4 class="list-title">Occupational Therapy</h4>
					<p>
						Our occupational therapists and assistants educate residents on strategies to manage the activities of daily living and teach them how to use adaptive equipment to increase their independence.
					</p>
					<ul>
						<li>Dressing</li>
						<li>Eating</li>
						<li>Bathing and showering</li>
						<li>Homemaking skills (laundry, washing dishes, etc.)</li>
						<li>Exercise programs</li>
						<li>Wheelchair positioning and mobility</li>
						<li>Splinting of extremities</li>
						<li>Money management</li> 
						<li>Home evaluations</li>
						<li>Environmental adaptations</li>
						<li>Family education and training</li>
					</ul>
				<h4 class="list-title">Speech Therapy</h4>
					<p>
						Our speech therapists help residents regain their cognitive and communication abilities. They are also certified in swallowing rehabilitation and can provide therapies to those suffering from Dysphagia or difficulty swallowing.
					</p>
					<ul>
						<li>Speech</li>
						<li>Voice / Vocal Quality</li>
						<li>Swallowing / Chewing / Eating</li>
						<li>Receptive language ability</li>
						<li>Expressive language ability </li>
						<li>Cognitive functioning</li>
					</ul>
			</article>
			<!-- /Article -->
			
			<!-- Sidebar -->
			<aside class="col-md-4 sidebar sidebar-right">
				<div class="row widget">
					<div class="col-xs-12">
						<p><img src="../assets/images/therapy.jpg" width="400" alt="Physical Therapy and Rehabilitative care at O'Neill Healthcare" /></p>
					</div>
				</div>
				<div class="row widget">
					<div class="col-xs-12">
						<p><img src="../assets/images/therapy2.jpg" width="400" alt="Physical Therapy and Rehabilitative care at O'Neill Healthcare" /></p>
					</div>
				</div>
			</aside><!-- /Sidebar -->
		</div>
	</div>	<!-- /container -->
	
	<footer id="footer" class="top-space">
		<div class="locfooter">
			<div class="container">
				<div class="row">
					<div class="col5 icol widget">
						<div class="locpan">
						<p><a href="../bayvillage.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../bayvillage.php">Bay Village</a></p>
						<p>
							605 Bradley Road<br>
							Bay Village, OH 44140<br> 
						</p>
						<p>
							440-871-3474
						</p>
						</div>
					</div>

					<div class="col5 icol widget ">
						<div class="locpan">
						<p><a href="../fairview.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../fairview.php">Fairview Park</a></p>
						<p>
							20770 Lorain Road<br>
							Fairview Park, OH 44126<br>
						</p>
						<p>
							440-331-0300
						</p>
						</div>
					</div>

					<div class="col5 icol widget">
						<div class="locpan">
						<p><a href="../lakewood.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../lakewood.php">Lakewood</a></p>						
						<p>
							13900 Detroit Avenue<br>
							Lakewood, OH 44107<br> 
						</p>
						<p>
							216-228-7650
						</p>
						</div>
					</div>

					<div class="col5 icol widget ">
						<div class="locpan">
						<p><a href="../northolmsted.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../northolmsted.php">North Olmsted</a></p>
						<p>
							4800 Clague Road<br>
							N Olmsted, OH 44070<br> 
						</p>
						<p>
							440-734-9933
						</p>
						</div>
					</div>

					<div class="col5 icol icol100 widget ">
						<div class="locpan">
							<p><a href="../northridgeville.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
							<p><a href="../northridgeville.php">North Ridgeville</a></p>
							<p>
								38600 Center Ridge Road<br>
								N Ridgeville, OH 44039<br>
							</p>
							<p>
								440-327-1295
							</p>
						</div>
					</div>
				</div> <!-- /row of widgets -->
			</div>
		</div>
		<?php include '../inc/add/footer.php'; ?>
	</footer>	
	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="../assets/js/headroom.min.js"></script>
	<script src="../assets/js/jQuery.headroom.min.js"></script>
	<script src="../assets/js/template.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55359204-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>
