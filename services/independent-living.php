<!DOCTYPE html>
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="author"      content="Milan (perceptionmm.com)">
	<meta name="description" content="Independent living at O'Neill Healthcare is an option for individuals who do not need personal care assistance with activities of daily living and only require intermittent attention from a nurse for emergencies.">
	<meta name="keywords" content="O'Neill Healthcare, O'Neill Managment, O'Neill Nursing Home, Bradley Bay, Center Ridge, Lakewood, Wellington, Independent Living, Cleveland, Cleveland Nursing Homes, Cleveland Healthcare" />

	<title>Independent Living - O'Neill Healthcare &amp; Managment</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="../favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="../favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="../favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="../favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="../favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="../favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="../favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="../favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="../favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="../favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="../favicon/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="../favicon/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="../favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="../favicon/browserconfig.xml">
	
	<!-- FancyBox -->
    <link rel="stylesheet" href="../assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="../assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="../assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="../assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="../assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lte IE 9]>
	<script src="../assets/js/html5shiv.js"></script>
	<script src="../assets/js/respond.min.js"></script>
	<link rel="stylesheet" href="../assets/css/ie.css">
	<![endif]-->

	<?php $page = "independentliving"; ?>
</head>


<body>
	<!-- Fixed navbar -->
	<?php include '../inc/add/nav.php'; ?>
	<!-- /.navbar -->

	<header id="head" class="independent"></header>

	<!-- container -->
	<div class="container">

		<ol class="breadcrumb">
			<li><a href="../index.php">Home</a></li>
			<li><a href="../services.php">Services</a></li>
			<li class="active">Independent Living</li>
		</ol>

		<div class="row">
			
			<!-- Article main content -->
			<article class="col-md-8 maincontent">
				<header class="page-header">
					<h1 class="page-title">Independent Living</h1>
				</header>

				
				
				<p>
					O’Neill Healthcare offers residential apartments that are ideal for people who are capable of navigating the activities of daily living but want to be a part of a supportive, friendly community.
				</p>

				<p>
					Residents can choose from one-or two-bedroom apartments nestled on our campus, conveniently located near the heart of downtown Lakewood close to shopping, restaurants, public transportation, and medical services.
				</p>

				<p>
					We do not require long-term lease commitments or entrance fees and residents have the flexibility to choose services, such as housekeeping and laundry, that suit their needs. Each unit includes a kitchen and we offer a customizable meal plan with up to three full meals per day.
				</p>

				<p>
					All residents enjoy complimentary breakfast and an open invitation to participate in our scheduled activities and outings, such as seasonal scenic tours and trips to area attractions.
				</p>

				<h4 class="list-title">
					Our services and amenities include:
				</h4>

				<div class="nursinglist">
					<ul>
						<li>24-hour emergency alert service and nurse on call</li>
						<li>A “Guardian Angel” daily check-in with residents to see if they need assistance</li>
						<li>Apartments include carpet, draperies, range, and refrigerator</li>
						<li>Each building is equipped with an elevator and coin-operated washers and dryers</li>
						<li>Resident and guest parking</li>
						<li>Private dining room for family gatherings or special occasions</li>
						<li>Snow and ice removal from roads and walks</li>
						<li>Maintenance of buildings, grounds, landscaping, and apartments</li>
						<li>All utilities except personal phone and cable television</li>
						<li>Beauty and Barber Salon</li>
						<li>Daily activities including exercise, arts and crafts, and educational programs</li>
						<li>Scheduled transportation for shopping, banking, community-sponsored outings, and events</li>
					</ul>
				</div>

				<p>
					Our professional staff is driven by one goal: to exceed our residents' expectations while providing the highest level of dignity and respect. Residents enjoy a high level of independence with the peace of mind that our extra security and attention afford.
				</p>


			</article>
			<!-- /Article -->
			
			<!-- Sidebar -->
			<aside class="col-md-4 sidebar sidebar-right">

				
				<div class="row widget">
					<div class="col-xs-12">
						
						
						
						<p><img src="../assets/images/independent.jpg" alt="Our resident gardening" /></p>
						
						
					</div>
				</div>
				
				

			</aside>
			<!-- /Sidebar -->

		</div>
	</div>	<!-- /container -->
	

	<footer id="footer" class="top-space">

		<div class="locfooter">
			<div class="container">
				<div class="row">
					
					


					<div class="col-md-4 col-md-offset-4 widget">
						<div class="locpan">
						<p><a href="../lakewood.php"><i class="fa fa-map-marker fa-4x"></i></a></p>
						<p><a href="../lakewood.php">Lakewood</a></p>						
						<p>
							13900 Detroit Avenue<br>
							Lakewood, OH 44107<br>
						</p>
						<p>
							216-228-7650
						</p>
						</div>
					</div>


					

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<?php include '../inc/add/footer.php'; ?>

	</footer>	
		




	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="../assets/js/headroom.min.js"></script>
	<script src="../assets/js/jQuery.headroom.min.js"></script>
	<script src="../assets/js/template.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55359204-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>