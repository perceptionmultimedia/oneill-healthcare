<!DOCTYPE html>
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="author"      content="Milan (perceptionmm.com)">
	<meta name="description" content="At Bay Village O'Neill Healthcare we work to promote a healthy and active lifestyle for each and every resident. ">
	<meta name="keywords" content="O'Neill Healthcare, O'Neill Managment, O'Neill Nursing Home, Bradley Bay, Assisted Living, Dialysis, Hospice, Independent Living, Memory Support, Rehab and Therapy, Skilled Nursing, Alzheimer's, Cleveland, Cleveland Nursing Homes, Cleveland Healthcare" />

	<title>Fairview Park - O'Neill Healthcare &amp; Managment</title>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="favicon/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="favicon/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="favicon/browserconfig.xml">
	
	<!-- FancyBox -->
    <link rel="stylesheet" href="assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lte IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<link rel="stylesheet" href="assets/css/ie.css">
	<![endif]-->

	<?php $page = "fp"; ?>

</head>

<body onload="initialize()">

	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
	  		var js, fjs = d.getElementsByTagName(s)[0];
	  		if (d.getElementById(id)) return;
	  		js = d.createElement(s); js.id = id;
	  		js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=557918634301713&version=v2.0";
	  		fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));

		
	</script>


	<!-- Fixed navbar -->
	<?php include 'inc/nav.php'; ?>
	<!-- /.navbar -->


	<!-- Header -->
	<header id="head" class="fairview">

	</header>
	<!-- /Header -->
			
			
			<!-- container -->
	<div class="container">

		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="locations.php">Locations</a></li>
			<li class="active">Fairview Park</li>
		</ol>
		
		<div class="row">
			
			<!-- Sidebar -->
			<aside class="col-md-4 sidebar sidebar-left">



				<div class="row widget">
					<div class="col-xs-12">
						<p><img src="assets/images/logos/fairview_logo.png" alt="O'Neill Healthcare Lakewood Logo" class="img-responsive full-img"></p>
						
					</div>
				</div>

				<div class="row widget">
					<div class="col-xs-12">
						<ul class="mini-nav row">
							
							<li class="call col-md-12">
								<a href="tel:+4403310300">
									<div class="btn btn-default btn-block">
										<div class="phonehide">
											<i class="fa fa-phone fa-2"></i>    Call 
										</div>
										<div class="phone-num">440-331-0300</div>
									</div> 
								</a>
								
							</li>

							<li class="visit col-md-12" >
								<a href="#map_canvas">
								<div class="btn btn-default btn-block">
										<i class="fa fa-location-arrow fa-2"></i>    Visit
								</div>
								</a>
							</li>

							<li class="write col-md-12">
								<div class="btn btn-default btn-block">
									<i class="fa fa-pencil fa-2"></i>     Write
								</div>
							</li>
						
						</ul>

						<div class="bbform form row">

		                  	<div class="form-group">

			                  	
			                  	
			                    <input class="form-control" type="text" name="name" id="name" placeholder="Name">
			                   	
			                    <input class="form-control" type="text" name="email" id="email" placeholder="Email">

			                    <input class="form-control" type="text" name="phone" id="phone" placeholder="Phone">
			                    
			                    <textarea name="message" id="message" rows="7" class="form-control" placeholder="Comments / Questions"></textarea>
			                    <div id="result"></div>

		                	</div>

		                   	<button id="submit_btn" type="submit" class="btn btn-default btn-block">Submit</button>



						</div>
						
						
					</div>
				</div>
				<div class="row">
					<div class="widget col-sm-6 col-md-12 col-lg-12">

						<img src="assets/images/fairview_large_room.jpg" alt="Fairview Room" class="img-responsive full-img"/>
					</div>
					
					<div class="widget col-sm-6 col-md-12 col-lg-12">

						<img src="assets/images/PT_Transfers.jpg" alt="Fairview Transfer Service" class="img-responsive full-img"/>
						

					</div>

				</div>
				
				

			</aside>
			<!-- /Sidebar -->

			<!-- Article main content -->
			<article class="col-md-8 maincontent locmaincontent">
				<header class="page-header">
					<h1 class="page-title centered">O’Neill Healthcare Fairview Park</h1>
				</header>

				<p>
					Family owned and operated for more than 50 years, O’Neill Healthcare has been providing comprehensive inpatient and outpatient rehabilitation and nursing care since the opening of its first facility in Bay Village in 1962.
				</p>

				<p>
					That commitment to service and quality led O’Neill Healthcare to construct one of the finest and most comprehensive health care and rehabilitation facilities in Northeast Ohio, the new O’Neill Healthcare Fairview Park.
				</p>

				<p>
					Designed from the ground up to serve the many and varied needs of those needing rehabilitation, short-term or long-term care, O’Neill Healthcare Fairview Park is a state-of-the-art nursing facility.
				</p>

				<p>
					Constructed on the former Garnett School site, O’Neill Healthcare Fairview Park is close to the city’s senior center, public transportation, a wide variety of shopping options, churches, the Cleveland Clinic’s Fairview Hospital, the Metroparks and many doctors’ and dentists’ offices.
				</p>

				<p>
					O’Neill Healthcare Fairview Park is the newest and most comprehensive rehabilitation and nursing care facility in the O’Neill Healthcare organization, and the first of its kind in this beautiful west-shore community. The Western Reserve-style building opened in April 2015 to fill a need in the community where the O’Neill family has a strong history, calling it home for many years.
				</p>

				<p>
					In fact, John O’Neill and his sister, Doreen O'Neill Ziska, grew up and went to school in Fairview Park. That is what helps make O’Neill Healthcare’s new Fairview Park facility so special to the family.
				</p>

				<p>
					All other O’Neill Healthcare facilities have been recognized as some of the finest health care centers in Northeastern Ohio, ranking in the top 10 percent of all nursing homes in the State of Ohio for resident satisfaction. The employees, doctors and specialists at O’Neill Healthcare Fairview Park, which opened in April 2015, will follow those same time-honored practices and traditions, which has resulted in such high levels of resident and family satisfaction.
				</p>

				<div class="row">
					<h4 class="list-title">
						Learn more about our services!
					</h4>

					<div class="col-sm-6">
						<div class="service-btn">
							<a href="services/skilled-nursing.php">
								<p>
									Skilled <br>Nursing
								</p>
							</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="service-btn">
							<a href="services/rehab-and-therapy.php">
								<p>
									Rehabilitative <br>Therapies
								</p>
							</a>
						</div>
					</div>
				</div>

				<div class="row">
					<h3 class="list-title">
						O’Neill Healthcare Fairview Park at a glance:
					</h3>

					<p>
						O’Neill Healthcare Fairview Park offers 118 skilled nursing beds. The expert staff focuses on maximizing residents’ strength, mobility and function so they can make a confident return to everyday life as soon as possible.
					</p>
					<p>
						O’Neill Healthcare Fairview Park offers 12 dialysis stations within the building, designed for daytime and nighttime treatments.
					</p>
					<p>
						O’Neill Healthcare Fairview Park features a coffee shop, movie theater, beauty and barber salon, a chapel with live-streaming religious services and spacious and brightly lit common areas.
					</p>
					<p>
						O’Neill Healthcare Fairview Park has a state-of-the-art rehabilitation facility that features true-to-life versions of a grocery store and apartment, complete with kitchen and bathroom. The facility also includes a full-size automobile. The therapy gym contains state-of-the-art equipment; everything from a Nu-Step recumbent bicycle to a Wii virtual reality gaming system. Using these tools, the licensed physical, occupational and speech therapists and their assistants are able to work with the resident and get them confident and back home as soon as possible.
					</p>
					<p>
						O’Neill Healthcare Fairview Park offers complete orthopedic rehabilitation services, including physician consultation, evaluation and post-surgical follow-ups.
					</p>
					<p>
						O’Neill Healthcare Fairview Park is close to shopping, public transportation, dining, Cleveland Clinic’s Fairview Park hospital, the Cleveland Metroparks, churches, the Great Northern shopping complex, hotels and other amenities.
					</p>
				</div>

				<div class="row quickinfo">
					
					<div class="col-sm-6">
						<p>
							O'Neill Healthcare Fairview Park<br>
							20770 Lorain Road<br>
							Fairview Park, OH 44126
						</p>
					</div>
					<div class="col-sm-6">
						<p>
							P 440.331.0300 <br>
							<a href="mailto:info@oneillhc.com">info@oneillhc.com</a>
						</p>
					</div>

				</div>
			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->


			

		</div>
	</div>	<!-- /container -->
	
	<div id="map_canvas"></div>
    <div id="directionsPanel"></div>

	<footer id="footer">

		<?php include 'inc/footer.php'; ?>
	</footer>	
		




	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
	
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
		var geocoder;
		var directionsService;
		var directionsDisplay;

		function initialize() {
		    var latlng = new google.maps.LatLng(41.449277, -81.849708);
		    var myOptions = {
		        zoom: 13,
		        center: latlng,
		        mapTypeId: google.maps.MapTypeId.ROADMAP,
		        		        styles: [
				{
					featureType: "landscape",
					stylers: [
					{ color: "#476B00" },
					{ saturation: -50 },
					{ lightness: 70 },
					{ visibility: "on" }
					]
				},{
					featureType: "poi",
					elementType: "labels",
					stylers: [
					{ color: "#194719"},
					{ visibility: "simplified" }
					]
				},{
					featureType: "road.highway",
					stylers: [
					{ saturation: -50 },
					{ visibility: "simplified" }
					]
				},{
					featureType: "road.arterial",
					stylers: [
					{ saturation: -50 },
					{ visibility: "on" }
					]
				},{
					featureType: "road.local",
					stylers: [
					{ saturation: -50 },
					{ lightness: 40 },
					{ visibility: "on" }
					]
				},{
					featureType: "transit",
					stylers: [
					{ saturation: 0 },
					{ visibility: "simplified" }
					]
				},{
					featureType: "administrative.province",
					stylers: [
					{ visibility: "off" }
					]
			/** /
				},{
					featureType: "administrative.locality",
					stylers: [
						{ visibility: "off" }
					]
				},{
					featureType: "administrative.neighborhood",
					stylers: [
						{ visibility: "on" }
					]
					/**/
				},{
					featureType: "water",
					elementType: "labels",
					stylers: [
					{ visibility: "on" },
					{ lightness: -25 },
					{ saturation: -100 }
					]
				},{
					featureType: "water",
					elementType: "geometry",
					stylers: [
					{ color: "#75D1FF"},
					]
				}
				],
		    };

		    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		    var marker = new google.maps.Marker({
		        position: new google.maps.LatLng(41.449277, -81.849708),
		        map: map,
		        title: 'Bradley bay',
		        clickable: true
		    });

		    var infowindow = new google.maps.InfoWindow({
		        content: "Your address: <input id='clientAddress' type='text'>"+
		                "<input type='button' onClick=getDir() value='Go!'>"
		    });

		    google.maps.event.addListener(marker, 'click', function () {
		        infowindow.open(map, marker);
		    });
		    geocoder = new google.maps.Geocoder();
		    directionsService = new google.maps.DirectionsService();
		    directionsDisplay = new google.maps.DirectionsRenderer({
		        suppressMarkers: false
		    });
		    directionsDisplay.setMap(map);
		    
		    directionsDisplay.setPanel(document.getElementById("directionsPanel"));
		   	
		}

		function getDir() {
		    geocoder.geocode({
		        'address': document.getElementById('clientAddress').value
		    },

		    function (results, status) {
		        if (status == google.maps.GeocoderStatus.OK) {
		            var origin = results[0].geometry.location;
		            var destination = new google.maps.LatLng(41.449277, -81.849708);

		            var request = {
		                origin: origin,
		                destination: destination,
		                travelMode: google.maps.DirectionsTravelMode.DRIVING
		            };

		            directionsService.route(request, function (response, status) {
		                if (status == google.maps.DirectionsStatus.OK) {
		                    directionsDisplay.setDirections(response);
		                }
		            });

		        } else {
		            document.getElementById('clientAddress').value =
		                "Directions cannot be computed at this time.";
		        }
		    });
		}
    </script>

    <script>
	
		$(".phone-num").hide();
		$(".call").click(function(){
			$(".phone-num").show();
			$(".phonehide").hide();
		});

		$(".bbform").hide();
		$(".write").click(function(){
			$(".mini-nav").css("border-bottom" , "none");
			$(".bbform").slideDown(1000);

		});
	</script>

	<!-- Google Maps 
	<script src="https://maps.googleapis.com/maps/api/js?key=&amp;sensor=false&amp;extension=.js"></script> 
	<script src="assets/js/google-map.js"></script>
	-->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55359204-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>