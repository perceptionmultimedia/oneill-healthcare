<?php
/** 
 * define what additional modules to install and which 
 * folders to place them in
 */
function installer_basic_modules()
{
    // note in addition to these modules Modules, Blocks, Users, Permissions & Groups are all
    // installed - zikula will not start without these
    // Category references
    // -------------------
    // _ADMIN_CATEGORY_00_a: System
    // _ADMIN_CATEGORY_01_a: Layout
    // _ADMIN_CATEGORY_02_a: Users
    //  ADMIN_CATEGORY_03_a: Content
    //  ADMIN_CATEGORY_04_a: 3rdParty
    //  ADMIN_CATEGORY_05_a: Security
    //  ADMIN_CATEGORY_06_a: Hookd
    return array(array('module'   => 'Admin_Messages',
                       'category' => _ADMIN_CATEGORY_03_a),
                 array('module'   => 'SecurityCenter',
                       'category' => _ADMIN_CATEGORY_05_a),
                 array('module'   => 'Tour',
                       'category' => _ADMIN_CATEGORY_03_a),
                 array('module'   => 'Categories',
                       'category' => _ADMIN_CATEGORY_03_a),
                 array('module'   => 'Header_Footer',
                       'category' => _ADMIN_CATEGORY_01_a),
                 array('module'   => 'legal',
                       'category' => _ADMIN_CATEGORY_03_a),
                 array('module'   => 'Mailer',
                       'category' => _ADMIN_CATEGORY_00_a),
                 array('module'   => 'Errors',
                       'category' => _ADMIN_CATEGORY_00_a),
                 array('module'   => 'pnRender',
                       'category' => _ADMIN_CATEGORY_01_a),
                 array('module'   => 'pnForm',
                       'category' => _ADMIN_CATEGORY_00_a),
                 array('module'   => 'Search',
                       'category' => _ADMIN_CATEGORY_03_a),
                 array('module'   => 'Workflow',
                       'category' => _ADMIN_CATEGORY_00_a),
                 array('module'   => 'PageLock',
                       'category' => _ADMIN_CATEGORY_00_a),
                 array('module'   => 'SysInfo',
                       'category' => _ADMIN_CATEGORY_05_a));
}

/** 
 * Custom configuration for modules installed by this install type
 */
function installer_basic_post_install()
{
    // no custom configuration for this install type
    return;
}
