<?php
/**
 * Zikula Application Framework
 *
 * @copyright (c) 2001, Zikula Development Team
 * @link http://www.zikula.org
 * @version $Id: functions.php 24522 2008-08-04 07:59:15Z markwest $
 * @license GNU/GPL - http://www.gnu.org/copyleft/gpl.html
 * @author Gregor J. Rothfuss
 */

function install()
{
    // configure our installation environment
    // no time limit since installation might take a while
    // error reporting level for debugging
    ini_set('max_execution_time' , 86400);

    define('_PNINSTALLVER', '1.0.2');

    // start the basics of PN
    include 'install/modify_config.php';
    include 'includes/pnAPI.php';
    pnInit(PN_CORE_ALL & ~PN_CORE_THEME & ~PN_CORE_MODS & ~PN_CORE_LANGS & ~PN_CORE_SESSIONS & ~PN_CORE_TOOLS & ~PN_CORE_AJAX);

    // load the languages language file
    include 'language/languages.php';
    include 'includes/pnobjlib/LanguageUtil.class.php';

    // get our input
    $vars = array('lang', 'installtype', 'dbhost', 'dbusername', 'dbpassword', 'dbname', 'dbprefix',
                  'dbtype', 'dbtabletype', 'createdb', 'realname', 'username', 'password',
                  'repeatpassword', 'email', 'url', 'action', 'loginuser', 'loginpassword',
                  'defaultmodule', 'defaulttheme');

    foreach ($vars as $var) {
        // in the install we're sure we don't wany any html so we can be stricter than
        // the FormUtil::getPassedValue API
        $$var = strip_tags(stripslashes(FormUtil::getPassedValue($var, '', 'POST')));
    }

    // login to supplied admin credentials
    if (empty($action) && check_installed()) { // need auth because pn is already installed.
        pnInit(PN_CORE_SESSIONS);
        if (pnUserLoggedIn()) {
            if (!SecurityUtil::checkPermission('.*', '.*', ACCESS_ADMIN)) {
                pnUserLogOut(); // unpriv user so boot them.
                $action = 'login';
            }
        } else { // login failed
            $action = 'login';
        }
    }

    // check for an empty action - if so then show the first installer  page
    if (empty($action)) {
        $action = 'lang';
    }

    // define our smarty object
    $smarty = new Smarty();
    $smarty->left_delimiter = '<!--[';
    $smarty->right_delimiter = ']-->';
    $smarty->compile_dir = 'pnTemp/pnRender_compiled';
    $smarty->template_dir = 'install/pntemplates';
    $smarty->plugins_dir = array('plugins', 'install/pntemplates/plugins', 'system/pnRender/plugins', 'system/Theme/plugins');

    // load the installer language files
    if (empty($lang)) {
        $cnvlanguage  = cnvlanguagelist();
        $currentlang  = '';
        $browserlangs = split ('[,;]', isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '');
        // attempt to match a browser language to an existing language pack
        foreach ($browserlangs as $blang) {
            if (isset($cnvlanguage[$blang]) && file_exists('language/' . DataUtil::formatForOS($cnvlanguage[$blang]) . '/core.php')) {
                $lang = DataUtil::formatForOS($cnvlanguage[$blang]);
                break;
            }
        }
    }
    if (file_exists("install/pnlang/$lang/global.php") && file_exists("language/$lang/core.php")) {
        include "install/pnlang/$lang/global.php";
        include "language/$lang/core.php";
    } else {
        include 'install/pnlang/eng/global.php';
        include 'language/eng/core.php';
    }

    // assign the values from config.php
    $smarty->assign($GLOBALS['PNConfig']['System']);

    // perform tasks based on our action
    switch ($action) {
        case 'licence' :
            $smarty->assign('licence', file_get_contents('docs/COPYING.txt'));
            break;
        case 'installtype' :
            // protection again anonymous login's (will connect by install will fail) - drak
            if (empty($dbname) || empty($dbusername)){
                $action = 'dbinformation';
                $smarty->assign('dbcreatefailed', true);
            } else {
                update_config_php($dbhost, $dbusername, $dbpassword, $dbname, $dbprefix, $dbtype, $dbtabletype);
                $dbconn = pnDBGetConn(true);
                if ($createdb) {
                    $result = makedb($dbtype, $dbhost, $dbusername, $dbpassword, $dbname);
                    // check if we've successfully made the db
                    if ($result != 2) {
                        $action = 'dbinformation';
                        $smarty->assign('dbcreatefailed', true);
                    }
                    //pnInit(PN_CORE_DB);
                }
            }

            // Must reinitialize the database since settings have changed as a result of the install process.
            // We do this manually because the API doesn't allow for pnInit to be called multiple times with different info in config.php
            // Probably a better way of doing this?
            $pnconfig = array();
            $pndebug = array();

            require 'config/config.php';
            $GLOBALS['PNConfig'] = $PNConfig;

            // Decode encoded new DB parameters
            $ak = array_keys($GLOBALS['PNConfig']['DBInfo']);
            foreach ($ak as $key) {
                if ($GLOBALS['PNConfig']['DBInfo'][$key]['encoded']) {
                    $GLOBALS['PNConfig']['DBInfo'][$key]['dbuname'] = base64_decode($GLOBALS['PNConfig']['DBInfo'][$key]['dbuname']);
                    $GLOBALS['PNConfig']['DBInfo'][$key]['dbpass'] = base64_decode($GLOBALS['PNConfig']['DBInfo'][$key]['dbpass']);
                    $GLOBALS['PNConfig']['DBInfo'][$key]['encoded'] = 0;
                }
            }

            pnDBInit();

            // we should be connected to the db at this stage
            $dbconn = pnDBGetConn(true);
            if (!$dbconn) {
                $action = 'dbinformation';
                $smarty->assign('dbconnectfailed', true);
                $smarty->assign(array('dbhost' => $dbhost, 'dbtype' => $dbtype, 'dbname' => $dbname, 'dbusername' => $dbusername,
                                'dbpassword' => $dbpassword, 'dbtableprefix' => $dbprefix));
            }
            break;
        case 'createadmin' :
            installmodules('basic', $lang);
            if ($installtype != 'basic') {
                installmodules($installtype, $lang);
            }
            break;
        case 'login' :
            if (empty($loginuser) && empty($loginpassword)) {
            } elseif (pnUserLogIn($loginuser, $loginpassword, false)) {
                if (!SecurityUtil::checkPermission('.*', '.*', ACCESS_ADMIN)) {
                    // not admin user so boot
                    pnUserLogOut();
                    $action = 'login';
                    $smarty->assign(array('loginstate' => 'notadmin'));
                } else {
                    $action = 'lang';
                }
            } else {
                // not a valid user
                $smarty->assign(array('loginstate' => 'failed'));
            }
            break;
        case 'selecttheme' :
            pnConfigSetVar('startpage', $defaultmodule);
            break;
        case 'selectmodule' :
            // fix for #15351 - prepend protocol to $url if needed
            if (!empty($url) && substr($url, 0, 7) != 'http://' && substr($url, 0, 8) != 'https://') {
                $url = 'http://' . $url;
            }

            if ($password !== $repeatpassword) {
                $action = 'createadmin';
                $smarty->assign('passwordcomparefailed', true);
                $smarty->assign(array('realname' => $realname, 'username' => $username, 'password' => $password, 'repeatpassword' => $repeatpassword, 'email' => $email, 'url' => $url));
            } elseif (!pnVarValidate($email, 'email')) {
                $action = 'createadmin';
                $smarty->assign('emailvalidatefailed', true);
                $smarty->assign(array('realname' => $realname, 'username' => $username, 'password' => $password, 'repeatpassword' => $repeatpassword, 'email' => $email, 'url' => $url));
            } elseif (!empty($url) && !pnVarValidate($url, 'url')) {
                $action = 'createadmin';
                $smarty->assign('urlvalidatefailed', true);
                $smarty->assign(array('realname' => $realname, 'username' => $username, 'password' => $password, 'repeatpassword' => $repeatpassword, 'email' => $email, 'url' => $url));
            } elseif ((!$username) || !(!preg_match("/[[:space:]]/", $username)) || !pnVarValidate($username, 'uname')) {
                $action = 'createadmin';
                $smarty->assign('uservalidatefailed', true);
                $smarty->assign(array('realname' => $realname, 'username' => $username, 'password' => $password, 'repeatpassword' => $repeatpassword, 'email' => $email, 'url' => $url));
            } else {
                // create our new site admin
                // TODO test the call the users module api to create the user
                //pnModAPIFunc('Users', 'user', 'finishnewuser', array('name' => $realname, 'uname' => $username, 'email' => $email, 'url' => $url, 'pass' => $password));
                createuser($realname, $username, $password, $email, $url);

                // add admin email as site email
                pnConfigSetVar('adminmail', $email);

                update_installed_status();
            }
            break;
        case 'gotosite' :
            pnInit(PN_CORE_THEME);
            pnConfigSetVar('Default_Theme', $defaulttheme);
            pnModAPIFunc('Theme', 'admin', 'regenerate');
            $smarty->assign('credits', file_get_contents('docs/CREDITS.txt'));
            break;
    }

    // assign some generic variables
    $smarty->assign(compact($vars));

    // check our action template exists
    $action = DataUtil::formatForOS($action);
    if ($smarty->template_exists("installer_$action.htm")) {
        $smarty->assign('action', $action);
        $templatename = "install/pntemplates/installer_$action.htm";
    } else {
        $smarty->assign('action', 'error');
        $templatename = 'install/pntemplates/installer_error.htm';
    }

    // at this point we now have all the information requried to display
    // the output. We don't use normal smarty functions here since we
    // want to avoid the need for a template complilation directory
    // TODO: smarty kicks up some odd errors when eval'ing templates
    // this way so the evaluation is suppressed.

    // get and evaluate the action specific template and assign to our
    // main smarty object as a new template variable
    $template = file_get_contents($templatename);
    $smarty->_compile_source('evaluated template', $template, $_var_compiled);
    ob_start();
    @$smarty->_eval('?>' . $_var_compiled);
    $_includecontents = ob_get_contents();
    ob_end_clean();
    $smarty->assign('maincontent', $_includecontents);

    // get and evaluate the page template
    $template = file_get_contents('install/pntemplates/installer_page.htm');
    $smarty->_compile_source('evaluated template', $template, $_var_compiled);
    ob_start();
    @$smarty->_eval('?>' . $_var_compiled);
    $_contents = ob_get_contents();
    ob_end_clean();

    // echo our final result - the combination of the two templates
    echo $_contents;
}

/**
 * Creates the DB on new install
 *
 * This function creates the DB on new installs
 *
 * @param string $dbconn Database connection
 * @param string $dbname Database name
 */
function makedb($dbtype, $dbhost, $dbusername, $dbpassword, $dbname)
{
    // make a new database - the adodb way
    $dbconn = ADONewConnection($dbtype);
    // note adodb's use of mysql_connect returns a warning so checking
    // $dbh afterwards won't prevent a warning being displayed
    // so for ease we suppress errors here
    $dbh    = @$dbconn->NConnect($dbhost, $dbusername, $dbpassword);
    if (!$dbh) {
        return false;
    }
    $dict   = NewDataDictionary($dbconn);
    $sql    = $dict->CreateDatabase($dbname);
    $return = $dict->ExecuteSQLArray($sql);
    return $return;
}

/**
 * This function inserts the default data on new installs
 */
function createuser($realname, $username, $password, $email, $url)
{
    // get the database connection
    pnModDBInfoLoad('Users', 'Users');
    pnModDBInfoLoad('Profile', 'Profile');
    $dbconn  = pnDBGetConn(true);
    $pntable = pnDBGetTables();

    // create the password hash
    $password = DataUtil::hash($password, pnModGetVar('Users', 'hash_method'));

    // prepare the data
    $realname = DataUtil::formatForStore($realname);
    $username = DataUtil::formatForStore($username);
    $password = DataUtil::formatForStore($password);
    $email = DataUtil::formatForStore($email);
    $url = DataUtil::formatForStore($url);

    // create the admin user
    $sql = "UPDATE $pntable[users]
            SET    pn_uname        = '$username',
                   pn_email        = '$email',
                   pn_pass         = '$password',
                   pn_activated    = '1',
                   pn_user_regdate = '".date("Y-m-d H:i:s", time())."',
                   pn_lastlogin    = '".date("Y-m-d H:i:s", time())."'
            WHERE  pn_uid   = 2";

    $result = $dbconn->Execute($sql);

    if (!$result) {
        return false;
    } else {
        pnUserSetVar('name', $realname, 2);
        pnUserSetVar('url',  $url,      2);
        return true;
    }
}

function installmodules($installtype='basic', $lang='eng')
{
    static $modscat;

    // Lang validation
    $lang = DataUtil::formatForOS($lang);

    // Load category constant
    $files = array();
    if (file_exists("system/Admin/pnlang/$lang/init.php")) {
        $files[] = "system/Admin/pnlang/$lang/init.php";
    } elseif (file_exists("system/Admin/pnlang/eng/init.php")) {
        $files[] = 'system/Admin/pnlang/eng/init.php';
    }
    Loader::loadOneFile($files);

    // load our installation configuration
    $installtype = DataUtil::formatForOS($installtype);
    if ($installtype == 'complete') {
    } elseif (file_exists("install/pninstalltypes/$installtype.php")) {
        include "install/pninstalltypes/$installtype.php";
        $func = "installer_{$installtype}_modules";
        $modules = $func();
    } else {
        return false;
    }

    // create a result set
    $results = array();

    if ($installtype == 'basic') {
        $coremodules = array('Modules', 'Permissions', 'Groups', 'Blocks', 'ObjectData', 'Users', 'Profile', 'Theme', 'Admin', 'Settings');
        // manually install the modules module
        foreach ($coremodules as $coremodule) {
            // sanity check - check if module is already installed
            if ($coremodule != 'Modules' && pnModAvailable($coremodule)) {
                continue;
            }
            if (file_exists($file = "system/$coremodule/pnlang/$lang/init.php")) {
                Loader::includeOnce($file);
            } elseif (file_exists($file = 'system/$coremodule/pnlang/eng/init.php')) {
                Loader::includeOnce($file);
            }
            pnModDBInfoLoad($coremodule, $coremodule);
            Loader::requireOnce("system/$coremodule/pninit.php");
            $modfunc = "{$coremodule}_init";
            if ($modfunc()) {
                $results[$coremodule] = true;
            }
        }

        pnUserLogin('Admin', 'Password', false);
        // regenerate modules list
        $filemodules = pnModAPIFunc('Modules', 'admin', 'getfilemodules');
        pnModAPIFunc('Modules', 'admin', 'regenerate', array('filemodules' => $filemodules));

        // set each of the core modules to active
        reset($coremodules);
        foreach ($coremodules as $coremodule) {
            $mid = pnModGetIDFromName($coremodule, true);
            pnModAPIFunc('Modules', 'admin', 'setstate', array('id' => $mid, 'state' => PNMODULE_STATE_INACTIVE));
            pnModAPIFunc('Modules', 'admin', 'setstate', array('id' => $mid, 'state' => PNMODULE_STATE_ACTIVE));
        }
        // Add them to the appropriate category
        reset($coremodules);

        $coremodscat = array('Modules'     => _ADMIN_CATEGORY_00_a,
        'Permissions' => _ADMIN_CATEGORY_02_a,
        'Groups'      => _ADMIN_CATEGORY_02_a,
        'Blocks'      => _ADMIN_CATEGORY_01_a,
        'ObjectData'  => _ADMIN_CATEGORY_00_a,
        'Users'       => _ADMIN_CATEGORY_02_a,
        'Profile'     => _ADMIN_CATEGORY_02_a,
        'Theme'       => _ADMIN_CATEGORY_01_a,
        'Admin'       => _ADMIN_CATEGORY_00_a,
        'Settings'    => _ADMIN_CATEGORY_00_a);

        $categories = pnModAPIFunc('Admin', 'admin', 'getall');
        $modscat = array();
        foreach($categories as $category) {
            $modscat[$category['catname']] = $category['cid'];
        }
        foreach($coremodules as $coremodule) {
            $category = $coremodscat[$coremodule];
            pnModAPIFunc('Admin', 'admin', 'addmodtocategory', array('module' => $coremodule, 'category' => $modscat[$category]));
        }
        // create the default blocks.
        Loader::requireOnce('system/Blocks/pninit.php');
        blocks_defaultdata();
    }
    pnConfigSetVar('language', $lang);

    if ($installtype == 'complete') {
        $modules = array();
        $mods = pnModAPIFunc('Modules', 'admin', 'list', array('state' => PNMODULE_STATE_UNINITIALISED));
        foreach ($mods as $mod) {
            if (!pnModAvailable($mod['name'])) {
                $modules[] = $mod['name'];
            }
        }
        foreach ($modules as $module) {
            $results[$module] = false;
            //Temporary until Multisites is in a releasable state (PC)
            if($module=="MultiSites")continue;

            $mid = pnModGetIDFromName($module);
            if (pnModAPIFunc('Modules', 'admin', 'initialise', array('id' => $mid))==true) {
                // activate it
                if (pnModAPIFunc('Modules', 'admin', 'setstate', array('id' => $mid, 'state' => PNMODULE_STATE_ACTIVE))) {
                    $results[$module] = true;
                }
            }
        }
    } else {
        foreach ($modules as $module) {

            // sanity check - check if module is already installed
            if (pnModAvailable($module['module'])) {
                continue;
            }

            $results[$module['module']] = false;

            // #6048 - prevent trying to install modules which are contained in an install type, but are not available physically
            if (!file_exists('system/' . $module['module'] . '/') && !file_exists('modules/' . $module['module'] . '/')) {
                continue;
            }

            $mid = pnModGetIDFromName($module['module']);

            // init it
            if (pnModAPIFunc('Modules', 'admin', 'initialise', array('id' => $mid))==true) {
                // activate it
                if (pnModAPIFunc('Modules', 'admin', 'setstate', array('id' => $mid, 'state' => PNMODULE_STATE_ACTIVE))) {
                    $results[$module['module']] = true;
                }
                // Set category
                pnModAPIFunc('Admin', 'admin', 'addmodtocategory', array('module' => $module['module'], 'category' => $modscat[$module['category']]));
            }
        }
    }

    // run any post-install routines
    $func = "installer_{$installtype}_post_install";
    if (function_exists($func)) {
        $func();
    }

    return $results;
}

function check_installed()
{
    // See if installed flag is set.

    // set default
    $isinstalled = false;

    $lines = file('config/config.php');
    if ($lines<>false) {
        foreach($lines as $line) {
            $line = str_replace(' ', '', $line);
            $isinstalled = stristr($line, '$PNConfig[\'System\'][\'installed\']=1;');
            if ($isinstalled !== false) {
                break;
            }
        }
    }
    return $isinstalled;
}
