<?php

// language defines for the upgrade process

// upgrade76.php
define('_UPG_PROCEEDTOUPGRADE', 'Proceed to Upgrade');
define('_UPG_TITLE', 'Zikula v1.0 series Upgrade script (for PostNuke version .764)');
define('_UPG_SUBTITLE', 'PostNuke Upgrade script (for versions .764 only)');
define('_UPG_DESCRIPTION', 'This script will upgrade PostNuke v0.764 to Zikula v1.0. Upgrades from prior releases of PostNuke are not supported by this script.');
define('_UPG_BACKUPNOTICE', 'BEFORE proceeding you should backup your database');
define('_UPG_BACKINGUPDB', 'Backing up user database... ');
define('_UPG_DONE', 'done');
define('_UPG_USERDBSTRUCTUREUPGRADED', 'Users database structure upgraded.');
define('_UPG_MODULEDBSTRUCTUREUPGRADED', 'Module database structure upgraded.');
define('_UPG_SUCCESSFUL', 'successful');
define('_UPG_FAILED', 'FAILED!');
define('_UPG_BLOCKSTABLEUPGRADED', 'Blocks tables upgraded');
define('_UPG_RESETTHEMETOEXTRALITE', 'Reset theme to ExtraLite');
define('_UPG_DUDMIGRATED', 'Dynamic User data migrated');
define('_UPG_SEARCHMODULEREINSTALLED', 'Search module reinstalled');
define('_UPG_FOOTERINFO', 'For more information about the upgrade process, please read the <a href="docs/upgrade.html">upgrade documentation</a>, visit our <a href="http://community.zikula.org/Wiki.htm">wiki</a> or the <a href="http://community.zikula.org/module-Forum.htm">support forum</a>');
define('_UPG_TIMEHINT', 'Caution: Depending on the number of users in your installation, this may take a while. If you are not sure, test the upgrade on a local installation first.');
define('_UPG_STARTUPGRADE', 'Click here to start the upgrade process.');
define('_UPG_USERSACTIVATED', 'Existing users have been activated');
define('_UPG_MEMHINT', 'The memory limit cannot be changed (from 64M), you may run into problems upgrading larger sites.');

// upgrade.php
define('_UPG_MODULELISTREGENERATED', 'Modules list regenerated');
define('_UPG_INSTALLNEWMODULES', 'Install new system modules');
define('_UPG_STARTNEWMODULEINSTALL', 'Starting installation of new system modules');
define('_UPG_INITIALISED', 'initialised');
define('_UPG_NOTINITIALISED', 'not initialised');
define('_UPG_NONEWMODSNEEDINIT', 'No new system modules required initialising');
define('_UPG_INSTALLOFMODULESCOMPLETE', 'Installation of new system modules complete');
define('_UPG_UPGRADEALLMODULES', 'Upgrade all modules');
define('_UPG_STARTINGUPGRADE', 'Starting upgrade');
define('_UPG_UPGRADED', 'upgraded');
define('_UPG_NOTUPGRADED', 'not upgraded');
define('_UPG_NOMODSNEEDUPGRADE', 'No modules required upgrading');
define('_UPG_FINISHEDUPGRADE', 'Finished upgrade - ');
define('_UPG_GOTOADMINPANEL', 'Go to the admin panel for <a href="%s">%s</a>');
define('_UPG_FORTHENEXTSTEPSPLEASELOGIN', 'For the next upgrade steps you need to be logged in. Please provide your admin account credentials');
define('_UPG_USERNAME', 'Username');
define('_UPG_PASSWORD', 'Password');
define('_UPG_SUBMIT', 'Submit');
define('_UPG_LOGINFAILED', 'Failed to login to your site');
